# Docker image for springboot file run
# VERSION 0.0.1
# Author: 张毅
# 基础镜像使用centos7
FROM centos:centos7
# 作者
MAINTAINER zhangyi
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime  \
    && echo $TZ > /etc/timezone \
    && yum -y install java-1.8.0-openjdk java-1.8.0-openjdk-devel \
    && yum -y install gcc gcc-c++ kernel-devel \
    && localedef -c -i en_US -f UTF-8 C.UTF-8

ENV LANG C.UTF-8
# VOLUME 指定了临时文件目录为/tmp。
# 其效果是在主机 /var/lib/docker 目录下创建了一个临时文件，并链接到容器的/tmp
VOLUME /tmp
# 将jar包添加到容器中并更名为xx.jar
ADD metasearch-search-0.0.1-SNAPSHOT.jar app.jar
# 运行jar包
# RUN bash -c 'touch /app.jar'
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]