package org.zp.search.pojo;

import java.util.ArrayList;
import java.util.HashMap;

public class SegTemplate {

    private Boolean suffix_res ;
    private String query_witout_suffix ;
    private ArrayList<String> query_kws_lst;
    private HashMap<String,ArrayList<String>> query_tags_kws;
    private HashMap<String,ArrayList<String>> query_kws_synons;
    private HashMap<String,HashMap<String,ArrayList<String>>> query_tags_synons;
    private HashMap<String,Double> query_kws_lst_idf;
    private ArrayList<String> ner;
    private double sum_idf_of_ner;
    private String cleaned_query ;
    private String cleaned_query_with_suffix ;
    private ArrayList<String> query_seg;

    public Boolean getSuffix_res() {
        return suffix_res;
    }

    public String getQuery_witout_suffix() {
        return query_witout_suffix;
    }

    public ArrayList<String> getQuery_kws_lst() {
        return query_kws_lst;
    }

    public HashMap<String, ArrayList<String>> getQuery_tags_kws() {
        return query_tags_kws;
    }

    public HashMap<String, ArrayList<String>> getQuery_kws_synons() {
        return query_kws_synons;
    }

    public HashMap<String, HashMap<String, ArrayList<String>>> getQuery_tags_synons() {
        return query_tags_synons;
    }

    public HashMap<String, Double> getQuery_kws_lst_idf() {
        return query_kws_lst_idf;
    }

    public ArrayList<String> getNer() {
        return ner;
    }

    public double getSum_idf_of_ner() {
        return sum_idf_of_ner;
    }

    public String getCleaned_query() {
        return cleaned_query;
    }

    public String getCleaned_query_with_suffix() {
        return cleaned_query_with_suffix;
    }

    public ArrayList<String> getQuery_seg() {
        return query_seg;
    }

    public void setSuffix_res(Boolean suffix_res) {
        this.suffix_res = suffix_res;
    }

    public void setQuery_witout_suffix(String query_witout_suffix) {
        this.query_witout_suffix = query_witout_suffix;
    }

    public void setQuery_kws_lst(ArrayList<String> query_kws_lst) {
        this.query_kws_lst = query_kws_lst;
    }

    public void setQuery_tags_kws(HashMap<String, ArrayList<String>> query_tags_kws) {
        this.query_tags_kws = query_tags_kws;
    }

    public void setQuery_kws_synons(HashMap<String, ArrayList<String>> query_kws_synons) {
        this.query_kws_synons = query_kws_synons;
    }

    public void setQuery_tags_synons(HashMap<String, HashMap<String, ArrayList<String>>> query_tags_synons) {
        this.query_tags_synons = query_tags_synons;
    }

    public void setQuery_kws_lst_idf(HashMap<String, Double> query_kws_lst_idf) {
        this.query_kws_lst_idf = query_kws_lst_idf;
    }

    public void setNer(ArrayList<String> ner) {
        this.ner = ner;
    }

    public void setSum_idf_of_ner(double sum_idf_of_ner) {
        this.sum_idf_of_ner = sum_idf_of_ner;
    }

    public void setCleaned_query(String cleaned_query) {
        this.cleaned_query = cleaned_query;
    }

    public void setCleaned_query_with_suffix(String cleaned_query_with_suffix) {
        this.cleaned_query_with_suffix = cleaned_query_with_suffix;
    }

    public void setQuery_seg(ArrayList<String> query_seg) {
        this.query_seg = query_seg;
    }
}
