package org.zp.search.configration;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.ThreadPoolExecutor;


/**
 * @author zhangyi
 */
@Configuration
public class MetaConfigration {
    //后续需要全部改成配置加载

    @Value("${elasticsearch.ip}")
    private String[] ips;

    @Value("${elasticsearch.connectRequestTimeout}")
    private int connectRequestTimeout;

    @Value("${elasticsearch.connectTimeout}")
    private int connectTimeout;

    @Value("${elasticsearch.socketTimeout}")
    private int socketTimeout;

    @Value("${elasticsearch.keepAliveStrategy}")
    private int keepAliveStrategy;

    @Value("${elasticsearch.maxConnTotal}")
    private int maxConnTotal;

    @Value("${elasticsearch.maxConnPerRoute}")
    private int maxConnPerRoute;

    @Bean
    public RestHighLevelClient buildRestHighLevelClient(){
        //HttpHost配置
        HttpHost[] hosts = Arrays.stream(ips)
                .map(this::makeHttohost)
                .filter(Objects::nonNull)
                .toArray(HttpHost[]::new);
        return
                new RestHighLevelClient(
                        RestClient.builder(hosts)
                                .setRequestConfigCallback(builder
                                        ->builder.setConnectionRequestTimeout(this.connectRequestTimeout)
                                        .setConnectTimeout(this.connectTimeout)
                                        .setSocketTimeout(this.socketTimeout)).setHttpClientConfigCallback(
                                        httpAsyncClientBuilder -> httpAsyncClientBuilder.setKeepAliveStrategy(
                                                ((httpResponse, httpContext) -> this.keepAliveStrategy )
                                        ).setMaxConnTotal(this.maxConnTotal).setMaxConnPerRoute(this.maxConnPerRoute)
                                )
                );
    }

    private HttpHost makeHttohost(String s){
        assert StringUtils.isNotEmpty(s);
        String[] address = s.split(":");
        if (address.length==2){
            String ip = address[0];
            int port = Integer.parseInt(address[1]);
            return new HttpHost(ip,port,"http");
        }else {
            return null;
        }
    }

    @Bean(name = "asyncPoolTaskExecutor")
    public ThreadPoolTaskExecutor executor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        //核心线程数
        taskExecutor.setCorePoolSize(20);
        //线程池维护线程的最大数量,只有在缓冲队列满了之后才会申请超过核心线程数的线程
        taskExecutor.setMaxPoolSize(100);
        //缓存队列
        taskExecutor.setQueueCapacity(50);
        //许的空闲时间,当超过了核心线程出之外的线程在空闲时间到达之后会被销毁
        taskExecutor.setKeepAliveSeconds(200);
        //异步方法内部线程名称
        taskExecutor.setThreadNamePrefix("MetaSearch-async-");
        /**
         * 当线程池的任务缓存队列已满并且线程池中的线程数目达到maximumPoolSize，如果还有任务到来就会采取任务拒绝策略
         * 通常有以下四种策略：
         * ThreadPoolExecutor.AbortPolicy:丢弃任务并抛出RejectedExecutionException异常。
         * ThreadPoolExecutor.DiscardPolicy：也是丢弃任务，但是不抛出异常。
         * ThreadPoolExecutor.DiscardOldestPolicy：丢弃队列最前面的任务，然后重新尝试执行任务（重复此过程）
         * ThreadPoolExecutor.CallerRunsPolicy：重试添加当前的任务，自动重复调用 execute() 方法，直到成功
         */
        taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        taskExecutor.initialize();
        return taskExecutor;
    }

}
