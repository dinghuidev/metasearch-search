package org.zp.search.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.zp.search.constants.MetaResponseVo;
import org.zp.search.constants.MetaWebStatusEnum;
import org.zp.search.service.MetaSearchService;
import org.zp.search.service.SearchService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * @author zhangyi
 */
@RestController
@Slf4j
public class SearchController {

    @Autowired
    private MetaSearchService metaSearchService;



    @RequestMapping(value = "/metasearch",method = RequestMethod.GET)
    public MetaResponseVo<Object> metasearch(@RequestParam(value = "query",required = true) String query){

        if (StringUtils.isNotEmpty(query)){
            String[] index = new String[]{"zp_baidu_v1","zp_bing_v1"};
            int searchSize = 30;
            if (query.length() >50){
                return new MetaResponseVo<>(MetaWebStatusEnum.SUCCESS.getCode(), MetaWebStatusEnum.SUCCESS.getDesc(),new ArrayList<>());
            }
            if (query.length() >30){
                query = query.substring(0,30);
            }
            List<Map<String, Object>> result = metaSearchService.search(query, index, searchSize,1);
            if (result != null){
                return new MetaResponseVo<>(MetaWebStatusEnum.SUCCESS.getCode(), MetaWebStatusEnum.SUCCESS.getDesc(), result);
            }
        }
        return new MetaResponseVo<>(MetaWebStatusEnum.FAILED.getCode(), MetaWebStatusEnum.FAILED.getDesc(), null);
    }
}
