package org.zp.search.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import org.zp.search.pojo.SegTemplate;
import org.zp.search.utils.BertSegmentUtil;
import org.zp.search.utils.HttpUtil;
import org.zp.search.utils.SimCalculateUtil;
import org.zp.search.utils.TextUtil;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @Author zhangyi
 * @Date 2023年04月16日 17:25
 * @Description 异步任务组件
 */
@Component
@Slf4j
public class MetaAsyncTask {

    @Autowired
    private BertSegmentUtil bertSegmentUtil;

    final String termWeightUrl = "http://36.103.203.41:8881/encode";
    final String embeddingUrl = "http://36.103.203.41:8882/encode";
    final String cutwordsUrl = "http://10.51.160.100:8080/kw";
    final String bertNliUrl = "http://36.103.203.41:8883/encode";

    @Async("asyncPoolTaskExecutor")
    public CompletableFuture<List<Float>> getTermWeight(String query) {
        long start = System.currentTimeMillis();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("is_tokenized", false);
        jsonObject.put("id", "123");
        jsonObject.put("texts", Collections.singletonList(query));
        String resStr = HttpUtil.postJson(termWeightUrl, jsonObject.toJSONString());
        long end = System.currentTimeMillis();
        log.info("query-{} MetaAsyncTask-getTermWeight-termWeight服务请求耗时结果为 {} ms", query, end - start);
        if (StringUtils.isNotEmpty(resStr)) {
            List<String> bertWords = bertSegmentUtil.segment(query);
//            log.info("bertWords 结果为{}",bertWords);
            List<Float> termWeight = new ArrayList<>();
            try {
                JSONArray scoreList = JSON.parseObject(resStr).getJSONArray("result").getJSONArray(0);
                for (int i = 1; i < bertWords.size() + 1; i++) {
                    Float score = scoreList.getJSONArray(i).getFloatValue(0);
                    for (int j = 0; j < bertWords.get(i - 1).length(); j++) {
                        termWeight.add(score);
                    }
                }
            } catch (Exception e) {
                log.error("[ERROR] query-{} MetaAsyncTask-getTermWeight-ParseTermWeightError, {}", query, e.toString());
            }
            return CompletableFuture.completedFuture(termWeight);
        }
        return CompletableFuture.completedFuture(null);
    }

    @Async("asyncPoolTaskExecutor")
    public CompletableFuture<JSONArray> getEmbedding(List<String> querys) {
        long start = System.currentTimeMillis();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("is_tokenized", false);
        jsonObject.put("id", "123");
        jsonObject.put("texts", querys);
        String resStr = HttpUtil.postJson(embeddingUrl, jsonObject.toJSONString());
        long end = System.currentTimeMillis();
        log.info("query-{} MetaAsyncTask-getEmbedding-embedding向量服务请求耗时结果为 {} ms", querys.get(0), end - start);

        if (StringUtils.isNotEmpty(resStr)) {
            try {
                JSONArray result = JSON.parseObject(resStr).getJSONArray("result");
                return CompletableFuture.completedFuture(result);
            } catch (Exception e) {
                log.error("[ERROR] query-{} MetaAsyncTask-getEmbedding-ParseEmbeddingError, {}", querys.get(0), e.toString());
            }
        }
        return CompletableFuture.completedFuture(null);
    }

    @Async("asyncPoolTaskExecutor")
    public CompletableFuture<SegTemplate> getCutWords(String query) {
        long start = System.currentTimeMillis();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("query", query);
        String resStr = null;
        try {
            resStr = HttpUtil.postJson(cutwordsUrl, jsonObject.toJSONString());
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaAsyncTask-GetCutWordsError, {}", query, e.getMessage());
        }
        long end = System.currentTimeMillis();
//        log.info("query-{} MetaAsyncTask-getCutWords-cutWords服务请求耗时结果为 {} ms", query, end - start);
        if (resStr == null) {
            return CompletableFuture.completedFuture(null);
        }
        JSONObject res = new JSONObject();
        SegTemplate segTemplate = new SegTemplate();
        try {
            res = JSON.parseObject(resStr);
            if (res.containsKey("result")) {
                JSONArray res1 = res.getJSONArray("result");
                segTemplate.setSuffix_res(res1.getBoolean(0));

                segTemplate.setQuery_witout_suffix(res1.getString(1));
                segTemplate.setQuery_kws_lst((ArrayList<String>) res1.getJSONArray(2).toJavaList(String.class));
                segTemplate.setQuery_tags_kws(JSON.parseObject(res1.getJSONObject(3).toJSONString(),
                        new TypeReference<HashMap<String, ArrayList<String>>>() {
                        }));
                segTemplate.setQuery_kws_synons(JSON.parseObject(res1.getJSONObject(4).toJSONString(),
                        new TypeReference<HashMap<String, ArrayList<String>>>() {
                        }));
                segTemplate.setQuery_tags_synons(JSON.parseObject(res1.getJSONObject(5).toJSONString(),
                        new TypeReference<HashMap<String, HashMap<String, ArrayList<String>>>>() {
                        }));
                segTemplate.setQuery_kws_lst_idf(JSON.parseObject(res1.getJSONObject(6).toJSONString(),
                        new TypeReference<HashMap<String, Double>>() {
                        }));
                segTemplate.setNer((ArrayList<String>) res1.getJSONArray(7).toJavaList(String.class));
                segTemplate.setSum_idf_of_ner(res1.getDouble(8));
                segTemplate.setCleaned_query(res1.getString(9));
                segTemplate.setCleaned_query_with_suffix(res1.getString(10));
                segTemplate.setQuery_seg((ArrayList<String>) res1.getJSONArray(11).toJavaList(String.class));
            } else {
                log.error("[ERROR] query-{} MetaAsyncTask-CutWordsResHasNoResult, result is {}", query, res);
                return CompletableFuture.completedFuture(null);
            }
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaAsyncTask-ParseCutWordsError, {}", query, e.getMessage());
            return CompletableFuture.completedFuture(null);
        }
        return CompletableFuture.completedFuture(segTemplate);
    }

    @Async("asyncPoolTaskExecutor")
    public Future<List<Double>> getBertNLI(String query, List<String> titles) {
        long start = System.currentTimeMillis();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("is_tokenized", false);
        jsonObject.put("id", "123");
        jsonObject.put("texts", titles.stream().map(x -> query + " ||| " + x).collect(Collectors.toList()));
        String resStr = HttpUtil.postJson(bertNliUrl, jsonObject.toJSONString());
        long end = System.currentTimeMillis();
        log.info("query-{} MetaAsyncTask-getBertNLI-bertNli服务请求耗时结果为 {} ms", query, end - start);

        if (StringUtils.isNotEmpty(resStr)) {
            try {
                List<Double> simRes = new ArrayList<>();
                JSONArray result = JSON.parseObject(resStr).getJSONArray("result");
                for (int i = 0; i < result.size(); i++) {
                    Double value = result.getJSONArray(i).getDoubleValue(1);
                    simRes.add(value);
                }
                return CompletableFuture.completedFuture(simRes);
            } catch (Exception e) {
                log.error("[ERROR] query-{} MetaAsyncTask-getBertNLI-ParseBertNLIError, {}", query, e.toString());
            }
        }
        return CompletableFuture.completedFuture(null);
    }

    public static void main(String[] args) {
        MetaAsyncTask metaAsyncTask = new MetaAsyncTask();
        ArrayList<String> objects = new ArrayList<>();
        String q = "海淀有哪些中学";
        String q1 = "海淀区所有的中学 - 新知百科";
        String q2 = "海淀有哪些中学";
        objects.add(q);
        objects.add(q1);
        objects.add(q2);
        CompletableFuture<JSONArray> embedding = metaAsyncTask.getEmbedding(objects);
        try {
            JSONArray embeddings = embedding.get(2000, TimeUnit.MILLISECONDS);
            List<Float> queryEmbedding = embeddings.getJSONArray(0).toJavaList(Float.class);
            List<Float> queryEmbedding1 = embeddings.getJSONArray(1).toJavaList(Float.class);
            List<Float> queryEmbedding2 = embeddings.getJSONArray(2).toJavaList(Float.class);
            SimCalculateUtil simCalculateUtil = new SimCalculateUtil();
            log.info("query={},doc={},sim={}", q, q1, simCalculateUtil.bertSim(embeddings.getJSONArray(0).toJavaList(Double.class), embeddings.getJSONArray(1).toJavaList(Double.class)));
            log.info("query={},doc={},sim={}", q, q1, TextUtil.similarity(queryEmbedding, queryEmbedding1));
            log.info("query={},doc={},sim={}", q, q2, TextUtil.similarity(queryEmbedding, queryEmbedding2));
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaSearchService-postFilter-get-embedding service error {}", "query", e);
        }
    }
}
