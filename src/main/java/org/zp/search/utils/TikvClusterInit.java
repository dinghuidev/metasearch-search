package org.zp.search.utils;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

@Component
@Log4j2
public class TikvClusterInit {
    JedisCluster jedisCluster = null;

    @Value("${jedis.ip}")
    private String[] ips={"10.51.231.154:6666"};

    private void init(String ip, int port) {

        JedisPoolConfig config = new JedisPoolConfig();

        config.setMaxTotal(50);
        config.setMaxIdle(50);
        config.setMinIdle(10);
        config.setMaxWaitMillis(1000);
        config.setTestOnBorrow(true);
        config.setTestOnReturn(true);

        HostAndPort[] hostAndPorts = Arrays.stream(ips)
                .map(this::makeHostAndPort)
                .filter(Objects::nonNull)
                .toArray(HostAndPort[]::new);

//        HostAndPort host = new HostAndPort(ip, port);

        if (jedisCluster == null) {
            try {
                jedisCluster = new JedisCluster(new HashSet<>(Arrays.asList(hostAndPorts)), config);
                log.info("Jedis Init success");
            } catch (Exception e) {
                System.out.println("Jedis init error! Please check IP and Port!");
                log.info("[ERROR] Jedis Init Error {}", e.getMessage());
                System.exit(-1);
            }
        }

    }

    private HostAndPort makeHostAndPort(String s){
        assert StringUtils.isNotEmpty(s);
        String[] address = s.split(":");
        if (address.length==2){
            String ip = address[0];
            int port = Integer.parseInt(address[1]);
            return new HostAndPort(ip,port);
        }else {
            return null;
        }
    }
    public JedisCluster getCluster(String ip, int port) {
        if (jedisCluster == null) {
            init(ip, port);
        }
        return jedisCluster;
    }

}
