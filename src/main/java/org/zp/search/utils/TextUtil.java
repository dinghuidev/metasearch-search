package org.zp.search.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.*;
import java.security.PublicKey;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author zhangyi
 * @Date 2023年04月18日 16:51
 * @Description
 */

@Component
public class TextUtil {

    /**
     * 比较两个字符串的相识度
     * 核心算法：用一个二维数组记录每个字符串是否相同，如果相同记为0，不相同记为1，每行每列相同个数累加
     * 则数组最后一个数为不相同的总数，从而判断这两个字符的相识度
     *
     * @param str
     * @param target
     * @return
     */
    private static int compare(String str, String target) {
        int d[][];              // 矩阵
        int n = str.length();
        int m = target.length();
        int i;                  // 遍历str的
        int j;                  // 遍历target的
        char ch1;               // str的
        char ch2;               // target的
        int temp;               // 记录相同字符,在某个矩阵位置值的增量,不是0就是1
        if (n == 0) {
            return m;
        }
        if (m == 0) {
            return n;
        }
        d = new int[n + 1][m + 1];
        // 初始化第一列
        for (i = 0; i <= n; i++) {
            d[i][0] = i;
        }
        // 初始化第一行
        for (j = 0; j <= m; j++) {
            d[0][j] = j;
        }
        for (i = 1; i <= n; i++) {
            // 遍历str
            ch1 = str.charAt(i - 1);
            // 去匹配target
            for (j = 1; j <= m; j++) {
                ch2 = target.charAt(j - 1);
                if (ch1 == ch2 || ch1 == ch2 + 32 || ch1 + 32 == ch2) {
                    temp = 0;
                } else {
                    temp = 1;
                }
                // 左边+1,上边+1, 左上角+temp取最小
                d[i][j] = min(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + temp);
            }
        }
        return d[n][m];
    }


    /**
     * 获取最小的值
     */
    private static int min(int one, int two, int three) {
        return (one = Math.min(one, two)) < three ? one : three;
    }

    /**
     * 获取两字符串的相似度
     */
    public static float getSimilarityRatio(String str, String target) {
        int max = Math.max(str.length(), target.length());
        return 1 - (float) compare(str, target) / max;
    }

    public static void main(String [] args){
//        float value =  getSimilarityRatio("西红柿炒鸡蛋怎么做","西红柿炒鸡蛋怎么做好吃");
//        float value =  getSimilarityRatio("西红柿炒鸡蛋怎么做","怎么做西红柿炒鸡蛋");
//        System.out.println("value:"+sigmoid(31.628498));
//        System.out.println("value:"+sigmoid(31.267864));
//        System.out.println("value:"+sigmoid(31.086872));
//        double[] d = {31.628498,31.267864,31.086872,29.504206};
//        String a = "我爱北京";
//        String b = "北京";
//        System.out.println(b.indexOf(a));
        ArrayList<Map<String, Object>> l = new ArrayList<>();

        HashMap<String, Object> map = new HashMap<>();
        map.put("link","abc");
        map.put("name","zhangyi");

        HashMap<String, Object> map1 = new HashMap<>();
        map1.put("link","abc");
        map1.put("name","zhangyiyy");
        HashMap<String, Object> map2 = new HashMap<>();
        map2.put("link","sdsf");
        map2.put("name","zhangyiyy");

        l.add(map);
        l.add(map1);
        l.add(map2);
        System.out.println(l);
        List<Map<String, Object>> link = distinctListMapByKeys(l, Collections.singletonList("name"));
        System.out.println(link);
//        System.out.println(Arrays.toString(softmax(d)));
    }


    /**
     * 集合去重
     * @author zhangyi
     * @date 2023/4/20 18:42
     * @param originMapList
     * @param keys
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    public static List<Map<String, Object>> distinctListMapByKeys(
            List<Map<String,Object>> originMapList, List<String> keys){

        if(CollectionUtils.isEmpty(originMapList)) return null;
        Map<String, Map<String, Object>> tempMap = new HashMap<>();
        for (Map<String, Object> originMap : originMapList) {
            StringBuilder objHashCode = new StringBuilder();
            for (String key : keys) {
                String value = originMap.get(key) != null ? originMap.get(key).toString()  : "";
                objHashCode.append(value.hashCode());
            }
            tempMap.put(objHashCode.toString(),originMap);
        }
        List<Map<String, Object>> valueList = new ArrayList<>(tempMap.values());
        return valueList;
    }

    public static double sigmoid(double src) {
        return 1.0 / ( 1 + Math.exp(-src));
    }

    public static double[] softmax(double[] input) {
        double total = Arrays.stream(input).map(Math::exp).sum();
        double[] output = new double[input.length];
        for (int i=0; i<input.length; i++) {
            output[i] = (Math.exp(input[i]) / total);
        }
        return output;
    }

    public static double similarity(List<Float> va, List<Float> vb) {
        if (va.size() > vb.size()) {
            int temp = va.size() - vb.size();
            for (int i = 0; i < temp; i++) {
                vb.add(0.0f);
            }
        } else if (va.size() < vb.size()) {
            int temp = vb.size() - va.size();
            for (int i = 0; i < temp; i++) {
                va.add(0.0f);
            }
        }

        int size = va.size();
        double simVal = 0;

        double num = 0;
        double den = 1;
        double powa_sum = 0;
        double powb_sum = 0;
        for (int i = 0; i < size; i++) {
            double a = Double.parseDouble(va.get(i).toString());
            double b = Double.parseDouble(vb.get(i).toString());

            num = num + a * b;
            powa_sum = powa_sum + (double) Math.pow(a, 2);
            powb_sum = powb_sum + (double) Math.pow(b, 2);
        }
        double sqrta = (double) Math.sqrt(powa_sum);
        double sqrtb = (double) Math.sqrt(powb_sum);
        den = sqrta * sqrtb;

        simVal = num / den;

        return simVal;
    }

}
