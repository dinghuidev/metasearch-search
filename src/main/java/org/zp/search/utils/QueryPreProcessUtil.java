package org.zp.search.utils;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Log4j2
public class QueryPreProcessUtil {
    private Pattern regexClean = Pattern.compile("[^a-zA-Z0-9\\u4e00-\\u9fa5\\.\\:]");
    private Pattern regexCleanDouban = Pattern.compile("[^a-zA-Z0-9\\u4e00-\\u9fa5\\.\\:\\(\\)\\（\\）]");
    Pattern regexEnglish1 = Pattern.compile("([a-zA-Z]+)([0-9\\u4e00-\\u9fa5\\s]+)(的解释|的意思|什么意思|怎么读)");
    Pattern regexEnglish2 = Pattern.compile("([a-zA-Z]+)(的解释|的意思|什么意思|怎么读)");

    public String queryClean (String query) {
        Matcher match = regexClean.matcher(query);
        String clearn_query = match.replaceAll("").toLowerCase();
        System.out.println("clearn_query "+clearn_query);
        if(clearn_query.length()==0)
            return clearn_query;
        clearn_query = handleSuffix(clearn_query);
        char[] E_pun = "~;[][][]-.',:,.!?[]()<>\"\"'%#@&1234567890<>".toCharArray();
        char[] C_pun =  "～；『』﹃﹄「」—·’、：，。！？【】（）《》“”‘％＃＠＆１２３４５６７８９０〈〉".toCharArray();
        List<String> epunList = new ArrayList<>();
        for (Character ele: E_pun) {
            epunList.add(ele.toString());
        }
        List<String> cpunList = new ArrayList<>();
        for (Character ele: C_pun) {
            cpunList.add(ele.toString());
        }
        for(int i=0; i<epunList.size(); i++) {
            clearn_query = clearn_query.replace(cpunList.get(i), epunList.get(i));
        }
        return clearn_query;
    }

    public String doubanTitleClean(String query) {
        Matcher match = regexCleanDouban.matcher(query);
        String clean_query = match.replaceAll("").toLowerCase();
        if (clean_query.length() == 0)
            return clean_query;
        clean_query = handleSuffix(clean_query);
        char[] E_pun = "~;[][][]-.',:,.!?[]()<>\"\"'%#@&1234567890<>".toCharArray();
        char[] C_pun = "～；『』﹃﹄「」—·’、：，。！？【】（）《》“”‘％＃＠＆１２３４５６７８９０〈〉".toCharArray();
        List<String> epunList = new ArrayList<>();
        for (Character ele : E_pun) {
            epunList.add(ele.toString());
        }
        List<String> cpunList = new ArrayList<>();
        for (Character ele : C_pun) {
            cpunList.add(ele.toString());
        }
        for (int i = 0; i < epunList.size(); i++) {
            clean_query = clean_query.replace(cpunList.get(i), epunList.get(i));
        }
        clean_query = clean_query.replaceAll("\\(.*?\\)|\\{.*?}|\\[.*?]|（.*?）", "");
        return clean_query;
    }


    public String handleSuffix (String query) {
        String[] moodParticle= {"嘛", "了", "呢", "吧", "啊", "阿", "啦", "唉", "呢", "吧", "哇", "呀", "吗",
                "哦", "噢", "喔", "嘿", "吁", "吖", "吆", "呜", "咔", "呶", "呣", "咝", "咯", "呗", "咩", "哎"};
        String[] moodParticle1={"一下"};
        String[] moodParticle2={"请介绍一下","介绍一下","介绍下","你了解","了解","你知道","你认识"};
        int queryLen = query.length();
        for (String word: moodParticle) {
            String lastWord = query.substring(queryLen-1);
            if (lastWord.equals(word)) {
                query = query.substring(0, queryLen-1);

            }
        }

        int queryLen1 = query.length();
        for (String word: moodParticle1) {
            String lastWord = query.substring(queryLen1-2);
            if (lastWord.equals(word)) {
                query = query.substring(0, queryLen1-2);

            }
        }

        for (String s : moodParticle2) {
            if (query.startsWith(s)){
                query = query.replaceAll(s, "");
            }
        }
        return query;
    }



    public List<Map<String, Object>> matchEnglishQuery (String query) {
        List<Map<String, Object>> res = new ArrayList<>();
        Matcher matchWords1 = regexEnglish1.matcher(query);
        Matcher matchWords2 = regexEnglish2.matcher(query);
        List<String> resMatch = new ArrayList<String>();
        while (matchWords1.find()) {
            resMatch.add(matchWords1.group(1));
            System.out.println(resMatch);
        }
        while (matchWords2.find()) {
            resMatch.add(matchWords2.group(1));
            System.out.println(resMatch);
        }
        Map<String, Object> tmp = new HashMap<>();
        if (resMatch.size()==1) {
            String word = resMatch.get(0);
            tmp = new HashMap<String, Object>() {{
                put("title", word + "-有道词典");
                put("content", new ArrayList<String>() {{
                    add(word + "的翻译_音标_读音_用法_例句");
                }});
                put("description", word + "的翻译_音标_读音_用法_例句");
                put("publish_time", null);
                put("image", new ArrayList<String>() {{
                    add("http://shared.ydstatic.com/images/mobile/logo-mobile-whole.gif");
                }});
                put("url", "https://m.youdao.com/dict?le=eng&q=" + word);
                put("finalScore", 1000.0f);
                put("siteName", "有道词典");
            }};
            res.add(tmp);
        }
        return res;
    }

    public static void main(String[] args) {
        QueryPreProcessUtil queryPreProcessUtil = new QueryPreProcessUtil();
        String text = queryPreProcessUtil.queryClean("中秋节吃什么");
        String text1 = queryPreProcessUtil.handleSuffix("姚笛介绍一下呗");

        System.out.println(text1);
    }
}
