package org.zp.search.utils;

import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;
import cern.jet.math.Functions;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.log4j.Log4j2;
import ml.dmlc.xgboost4j.java.Booster;
import ml.dmlc.xgboost4j.java.XGBoost;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 相似度计算
 * @author zhangyi
 * @date 2023/4/25 20:03
 */
@Component
@Log4j2
public class SimCalculateUtil {

    public BasicUtil basic_util = new BasicUtil();

    private Booster titlePredictor;

    private static DoubleMatrix2D weight;

    private static DoubleMatrix2D bias;

    static {
        try {
            List<List<Double>> weightList = new ArrayList<>();
            List<Double> biasList = new ArrayList<>();
            Resource resource = new ClassPathResource("data/wb_train39.json");
            InputStream inputStream = resource.getInputStream();
            String texts = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
            JSONObject wbJson = JSONObject.parseObject(texts);
            JSONArray wArray = wbJson.getJSONArray("w");
            JSONArray bArray = wbJson.getJSONArray("b");
            for (int i = 0; i < wArray.size(); i++) {
                List<Double> w_i = new ArrayList<>();
                for (int j = 0; j < wArray.getJSONArray(i).size(); j++) {
                    w_i.add(wArray.getJSONArray(i).getDouble(j));
                }
                weightList.add(new ArrayList<>(w_i));
            }
            for (int i = 0; i < bArray.size(); i++) {
                biasList.add(bArray.getDouble(i));
            }
            weight = list2DToMatrix(weightList);
            bias = listToMatrix(biasList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("weight ans bias initialize error");
        }

    }


    public SimCalculateUtil() {
        log.info("load synons!");
        try {
            basic_util.Load_Synons();
        } catch (Exception e) {
            log.error("load synons error");
        }
        log.info("load synons done");
        log.info("load idf!");
        try {
            basic_util.Load_Idf();
        } catch (Exception e) {
            log.error("load idf error");
        }
        log.info("load idf done");
        log.info("load language!");
        try {
            basic_util.Load_Languages();
        } catch (Exception e) {
            log.error("load language error");
        }
        log.info("load language done");
        log.info("load rankmodel!");
        if (titlePredictor == null) {
            titlePredictor = predictor_init("data/TitleRankModel4.model");
        }
        log.info("load rankmodel done");
    }

    private double sqrtMulti(List<Double> vector1, List<Double> vector2) {
        double result = 0;
        result = squares(vector1) * squares(vector2);
        result = Math.sqrt(result);
        return result;
    }

    // 求平方和
    private double squares(List<Double> vector) {
        double result = 0;
        for (Double i : vector) {
            result += i * i;
        }
        return result;
    }

    // 点乘法
    private double pointMulti(List<Double> vector1, List<Double> vector2) {
        double result = 0;
        try {
            assert vector1.size() == vector2.size();
            for (int i = 0; i < vector1.size(); i++) {
                result += vector1.get(i) * vector2.get(i);
            }
        } catch (Exception e) {
            log.error("[ERROR] query&titleVector is not equal, vector1Len is {} vector1Len is {}", vector1.size(), vector2.size());
        }
        return result;
    }

    // 求余弦相似度
    public double cosSim(List<Double> vector1, List<Double> vector2) {
        double result = 0;
        result = pointMulti(vector1, vector2) / sqrtMulti(vector1, vector2);
        result = 0.5 + 0.5 * result;
        return result;
    }

    public double accuracySim1(String query, String title) {
        List<String> queryList = new ArrayList<>();
        for (char i : query.toCharArray()) {
            queryList.add(String.valueOf(i));
        }
        List<String> titleList = new ArrayList<>();
        for (char i : title.toCharArray()) {
            titleList.add(String.valueOf(i));
        }
//        char [] queryList = query.toCharArray();
//        char [] titleList = title.toCharArray();
        double common = 0;
        for (String ele : queryList) {
            if (titleList.contains(ele)) {
                common += 1;
            }
        }
        return common / query.length();
    }

    public double accuracySim2(String query, String title) {
        List<String> queryList = new ArrayList<>();
        for (char i : query.toCharArray()) {
            queryList.add(String.valueOf(i));
        }
        List<String> titleList = new ArrayList<>();
        for (char i : title.toCharArray()) {
            titleList.add(String.valueOf(i));
        }
//        char [] queryList = query.toCharArray();
//        char [] titleList = title.toCharArray();
        double common = 0;
        for (String ele : queryList) {
            if (titleList.contains(ele)) {
                common += 1;
            }
        }
        return common / title.length();
    }

    public int editDistance(String word1, String word2) {
        int dp[][] = new int[word1.length() + 1][word2.length() + 1];
        for (int i = 0; i < word1.length() + 1; i++) {
            // 从i个字符变成0个字符，需要i步（删除）
            dp[i][0] = i;
        }
        for (int i = 0; i < word2.length() + 1; i++) {
            // 当从0个字符变成i个字符，需要i步(增加)
            dp[0][i] = i;
        }
        for (int i = 1; i < word1.length() + 1; i++) {
            for (int j = 1; j < word2.length() + 1; j++) {
                //当相同的时，dp[i][j] = dp[i - 1][j - 1]
                if (word1.charAt(i - 1) == word2.charAt(j - 1)) {
                    dp[i][j] = dp[i - 1][j - 1];
                } else {
                    //当不同的时候，我们需要求三种操作的最小值
                    //其中dp[i - 1][j - 1]表示的是替换，dp[i - 1][j]表示删除字符，do[i][j - 1]表示的是增加字符
                    dp[i][j] = 1 + Math.min(dp[i - 1][j - 1], Math.min(dp[i - 1][j], dp[i][j - 1]));
                }
            }
        }
        return dp[word1.length()][word2.length()];
    }

    // 最长重复子序列
    public int getLcsLen(String query, String title) {
        int result = 0;
        char[] str1 = query.toCharArray();
        char[] str2 = title.toCharArray();
        int len1, len2;
        len1 = str1.length;
        len2 = str2.length;
        int maxLen = Math.max(len1, len2);
        int[] max = new int[maxLen];// 保存最长子串长度的数组
        int[] maxIndex = new int[maxLen];// 保存最长子串长度最大索引的数组
        int[] c = new int[maxLen];
        int i, j;
        for (i = 0; i < len2; i++) {
            for (j = len1 - 1; j >= 0; j--) {
                if (str2[i] == str1[j]) {
                    if ((i == 0) || (j == 0))
                        c[j] = 1;
                    else
                        c[j] = c[j - 1] + 1;//此时C[j-1]还是上次循环中的值，因为还没被重新赋值
                } else {
                    c[j] = 0;
                }
                // 如果是大于那暂时只有一个是最长的,而且要把后面的清0;
                if (c[j] > max[0]) {
                    max[0] = c[j];
                    maxIndex[0] = j;
                    for (int k = 1; k < maxLen; k++) {
                        max[k] = 0;
                        maxIndex[k] = 0;
                    }
                }
                // 有多个是相同长度的子串
                else if (c[j] == max[0]) {
                    for (int k = 1; k < maxLen; k++) {
                        if (max[k] == 0) {
                            max[k] = c[j];
                            maxIndex[k] = j;
                            break; // 在后面加一个就要退出循环了
                        }
                    }
                }
            }
            int tempResult = 0;
            for (int temp : c) {
                tempResult = Math.max(tempResult, temp);
            }
            result = Math.max(result, tempResult);
        }
        return result;
    }

    public List<String> queryTitleCommonKws(List<String> queryKws, List<String> titleKws) {
        List<String> commonKwsSet = new ArrayList<>();
        for (String ele : queryKws) {
            if (titleKws.contains(ele)) {
                commonKwsSet.add(ele);
            }
        }
        return commonKwsSet;
    }


    public double bm25SimilarOfChars(List<String> query_seg, List<String> doc_seg, double avg_doc_len) {
        double score = 0.0;

        double EPSILON = 1.0;
        double PARAM_K1 = 2.0;
        double PARAM_K2 = 1.0;
        double PARAM_B = 0.75;

        int doc_len = doc_seg.size();
        for (String word : query_seg) {
            double idf = basic_util.idf.getOrDefault(word, 0.0);
//            System.out.println("idf" +  idf);
            double fi = 0.0;
            double qfi = 0.0;
            int doc_seg_contains_word = 0;
            boolean flag = false;
            if (idf == 0.0) {
                idf = EPSILON * basic_util.avg_idf;
            }
            if (doc_seg.contains(word)) {
                fi = 1.0;
            } else {
                if (basic_util.synons.containsKey(word)) {
                    for (String w_ : basic_util.synons.get(word)) {
                        if (doc_seg.contains(w_)) {
                            flag = true;
                            if (w_.length() > 1) {
                                fi = 1.0;
                                doc_seg_contains_word = 2;
                                break;
                            } else {
                                doc_seg_contains_word = 1;
                                fi = 0.5;
                            }
                        }
                    }
                }
                if (!flag) {
                    fi = 0.0;
                }
            }
            double r1_deno = fi + PARAM_K1 * (1 - PARAM_B + PARAM_B * doc_len / avg_doc_len);
            double r1 = 0.0;
            if (r1_deno != 0) {
                r1 = fi * (PARAM_K1 + 1) / r1_deno;
            }
            if (query_seg.contains(word)) {
                qfi = 1.0;
            } else {
                qfi = 0.0;
                if (doc_seg_contains_word == 2) {
                    qfi = 1.0;
                } else if (doc_seg_contains_word == 1) {
                    qfi = 0.5;
                }
            }
            double r2_deno = qfi + PARAM_K2;
            double r2 = 0.0;
            if (r2_deno != 0) {
                r2 = qfi * (PARAM_K2 + 1) / r2_deno;
            }
            score += (idf * r1 * r2);
        }
        return score;
    }

    public double bm25SimilarOfWords(List<String> query_seg, String doc_seg, double avg_doc_len) {
        double score = 0.0;

        double EPSILON = 1.0;
        double PARAM_K1 = 2.0;
        double PARAM_K2 = 1.0;
        double PARAM_B = 0.75;

        int doc_len = doc_seg.length();
        for (String word : query_seg) {
            double idf = basic_util.idf.getOrDefault(word, 0.0);
            double fi = 0.0;
            double qfi = 0.0;
            int doc_seg_contains_word = 0;
            boolean flag = false;
            if (idf == 0.0) {
                idf = EPSILON * basic_util.avg_idf;
            }
            if (doc_seg.contains(word)) {
                fi = 1.0;
            } else {
                if (basic_util.synons.containsKey(word)) {
                    for (String w_ : basic_util.synons.get(word)) {
                        if (doc_seg.contains(w_)) {
                            flag = true;
                            if (w_.length() > 1) {
                                fi = 1.0;
                                doc_seg_contains_word = 2;
                                break;
                            } else {
                                doc_seg_contains_word = 1;
                                fi = 0.5;
                            }
                        }
                    }
                }
                if (!flag) {
                    fi = 0.0;
                }
            }
            double r1_deno = fi + PARAM_K1 * (1 - PARAM_B + PARAM_B * doc_len / avg_doc_len);
            double r1 = 0.0;
            if (r1_deno != 0) {
                r1 = fi * (PARAM_K1 + 1) / r1_deno;
            }
            if (query_seg.contains(word)) {
                qfi = 1.0;
            } else {
                qfi = 0.0;
                if (doc_seg_contains_word == 2) {
                    qfi = 1.0;
                } else if (doc_seg_contains_word == 1) {
                    qfi = 0.5;
                }
            }
            double r2_deno = qfi + PARAM_K2;
            double r2 = 0.0;
            if (r2_deno != 0) {
                r2 = qfi * (PARAM_K2 + 1) / r2_deno;
            }
            score += (idf * r1 * r2);
        }
        return score;
    }

    public int qMatchTNersNum(List<String> queryNers, List<String> titleNers) {
        int result = 0;
        for (String i : queryNers) {
            if (titleNers.contains(i)) {
                result = result + 1;
            }
        }
        return result;
    }

    public int qMatchTNersNumWithSynons(List<String> queryNers, HashSet<String> titleKwsSynonsSet) {
        int result = 0;
        for (String i : queryNers) {
            if (titleKwsSynonsSet.contains(i)) {
                result = result + 1;
            }
        }
        return result;
    }

    public int qMatchTKwsNum(List<String> queryKwsSet, List<String> titleKwsSet) {
        int result = 0;
        for (String i : queryKwsSet) {
            if (titleKwsSet.contains(i)) {
                result = result + 1;
            }
        }
        return result;
    }

    public int qMatchTKwsNumWithSynons(List<String> queryKwsSet, HashSet<String> titleKwsSynonsSet) {
        int result = 0;
        for (String i : queryKwsSet) {
            if (titleKwsSynonsSet.contains(i)) {
                result = result + 1;
            }
        }
        return result;
    }

    private ArrayList<Integer> count_freq(List<String> kws, String s) {
        ArrayList<Integer> freq = new ArrayList<>();
        for (String k : kws) {
            freq.add(StringUtils.countMatches(s, k));
        }
        return freq;
    }

    public ArrayList<Object> simFeat(List<String> kws1, List<String> kws2, String s1, String s2) {
        List<Integer> kws1_freq = count_freq(kws1, s1);
        ArrayList<Integer> kws2_freq = count_freq(kws2, s2);
        HashMap<String, Integer> dict1 = new HashMap<>();
        HashMap<String, Integer> dict2 = new HashMap<>();
        HashSet<String> set1 = new HashSet<>();
        Double a_above = 0.0;
        Double a_below1 = 0.0;
        Double a_below2 = 0.0;
        Integer b_above = 0;
        Integer b_below = 0;
        Double a = 0.0;
        for (int i = 0; i < kws1.size(); i++) {
            dict1.put(kws1.get(i), kws1_freq.get(i));
            set1.add(kws1.get(i));
        }
        for (int i = 0; i < kws2.size(); i++) {
            dict2.put(kws2.get(i), kws2_freq.get(i));
            set1.add(kws2.get(i));
        }
        for (String x : set1) {
            if (dict1.containsKey(x) && dict2.containsKey(x)) {
                a_above += (dict1.get(x) * dict2.get(x));
            }
            if (dict1.containsKey(x)) {
                a_below1 += (dict1.get(x) * dict1.get(x));
            }
            if (dict2.containsKey(x)) {
                a_below2 += (dict2.get(x) * dict2.get(x));
            }
        }
//		if(id.equals("72e5ca9b9e983e1b3c37e48eb82289bc")){
//			log.info("{} {} {}",a_above,a_below1,a_below2);
//		}
        if (a_below1 != 0.0 && a_below2 != 0.0) {
            a = a_above / ((Math.sqrt(a_below1 * a_below2)));
        }
        if (kws2.size() > kws1.size()) {
            b_below = kws1.size();
        } else {
            b_below = kws2.size();
        }
        for (String ele : kws1) {
            if (kws2.contains(ele)) {
                b_above += 1;
            }
        }
//        kws1.retainAll(kws2);
//        b_above=kws1.size();
        if (b_above == 0) {
            return new ArrayList<Object>() {{
                add(0.0);
                add(kws1_freq);
            }};
        }
        ArrayList<Object> ret = new ArrayList<Object>();
        ret.add(a * ((b_above * 1.0) / (1.0 * b_below)));
        ret.add(kws1_freq);
        return ret;
    }

    private Booster predictor_init(String uri) {
        try {
            Resource classPathResource = new ClassPathResource(uri);
            InputStream inputStream = classPathResource.getInputStream();
            Booster predictor = XGBoost.loadModel(inputStream);
            return predictor;
        } catch (Exception e) {
            log.error("Model Load Convert Error,{}", e.getMessage());
        }
        return null;
    }

    public Booster getTitlePredictor() {
        return titlePredictor;
    }

    public Map<String, List<String>> tmpGetWsyn(List<String> titleKwsSet, Map<String, Object> titleFeaturesSyn) {
        Map<String, List<String>> result = new HashMap<>();
        for (Map.Entry<String, Object> titleFeaturesSynMap : titleFeaturesSyn.entrySet()) {
            String key = titleFeaturesSynMap.getKey();
            JSONArray value = (JSONArray) ((JSONArray) titleFeaturesSynMap.getValue()).get(0);
            List<String> synList = JSONArray.parseArray(value.toJSONString(), String.class);
            if (titleKwsSet.contains(key) && !(synList.contains(key))) {
                result.put(key, synList);
            }
        }
        return result;
    }


    public String getFormatTime(String publishTime) {
        Pattern regexTmp = Pattern.compile("([0-9]{4})[-]([0-9]{2})[-]([0-9]{2})");
        Matcher matchTmp = regexTmp.matcher(publishTime);
        if (matchTmp.find()) {
            return publishTime;
        }
        String res = null;
        if (publishTime.length() >= 4) {
            String[] yearList = {"1988", "1988", "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998",
                    "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010",
                    "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021"};
            int stringLen = publishTime.length();
            if (Arrays.asList(yearList).contains(publishTime)) {
                res = publishTime + "-01-01";
            } else if (Arrays.asList(yearList).contains(publishTime.substring(0, 4)) && stringLen <= 8 && publishTime.matches("[0-9]+")) {
                Integer mon = null;
                Integer day = null;
                try {
                    mon = Integer.parseInt(publishTime.substring(4, 6));
                    day = Integer.parseInt(publishTime.substring(6, 8));
                } catch (Exception e) {
                }
                if (mon != null && mon >= 1 && mon <= 12 && day == null) {
                    res = publishTime.substring(0, 4) + "-" + mon.toString() + "-01";
                } else if (mon != null && mon >= 1 && mon <= 12 && day != null && day >= 1 && day <= 31) {
                    res = publishTime.substring(0, 4) + "-" + mon.toString() + "-" + day.toString();
                }
            } else {
                Pattern regexDate = Pattern.compile("([0-9]{4})[年]([0-9]+)[月]([0-9]+)");
                Matcher matchWords = regexDate.matcher(publishTime);
//                List<String> matchRes = new ArrayList<String>();
                Integer year = null;
                Integer mon = null;
                Integer day = null;
                try {
                    while (matchWords.find()) {
                        year = Integer.parseInt(matchWords.group(1));
                        mon = Integer.parseInt(matchWords.group(2));
                        day = Integer.parseInt(matchWords.group(3));
                    }
                } catch (Exception e) {
                }
//                System.out.println("test"+publishTime + "," + year+ "," + mon+ "," + day);
                if (year != null && Arrays.asList(yearList).contains(year.toString()) && mon == null) {
                    res = year.toString() + "-01-01";
                } else if (year != null && Arrays.asList(yearList).contains(year.toString()) && mon != null &&
                        mon >= 1 && mon <= 12 && day == null) {
                    res = year.toString() + "-" + mon.toString() + "-01";
                } else if (year != null && Arrays.asList(yearList).contains(year.toString()) && mon != null &&
                        mon >= 1 && mon <= 12 && day != null && day >= 1 && day <= 31) {
                    res = year.toString() + "-" + mon.toString() + "-" + day.toString();
                }

            }
        }
        if (res != null) {
            return res;
        } else {
            return publishTime;
        }
    }

    private static DoubleMatrix2D listToMatrix(List<Double> vector) {
        int size = vector.size();
        DoubleMatrix2D matrix = new DenseDoubleMatrix2D(1, size);
        double[][] array2D = new double[1][size];
        for (int i = 0; i < size; i++)
            array2D[0][i] = vector.get(i);
        matrix.assign(array2D);
        return matrix;
    }

    private static DoubleMatrix2D list2DToMatrix(List<List<Double>> list2D) {
        int m = list2D.size();
        int n = list2D.get(0).size();
        DoubleMatrix2D matrix = new DenseDoubleMatrix2D(m, n);
        double[][] array2D = new double[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                array2D[i][j] = list2D.get(i).get(j);
            }
        }
        matrix.assign(array2D);
        return matrix;
    }

    public double bertSim(List<Double> vector1, List<Double> vector2) {

        double result = 0;
        try {
            DoubleFactory2D F = DoubleFactory2D.dense;
            DoubleMatrix2D matrix1 = listToMatrix(vector1);
            DoubleMatrix2D matrix1Copy = matrix1.copy();
            DoubleMatrix2D matrix2 = listToMatrix(vector2);
            matrix1Copy.assign(matrix2, Functions.minus).assign(Functions.abs);
            DoubleMatrix2D x = F.appendColumns(F.appendColumns(matrix1, matrix2), matrix1Copy);
            DoubleMatrix2D y = new DenseDoubleMatrix2D(x.rows(), weight.rows());
            x.zMult(Algebra.DEFAULT.transpose(weight), y);
            y.assign(bias, Functions.plus);
            double[][] res = y.toArray();
            double[] prob = softMax(res[0]);
            result = prob[1];
        } catch (Exception e) {
            log.error("[ERROR] SimCalculateUtil-bertSim-Matrix Calculation Error,{}", e.getMessage());
        }
        return result;
    }

    public double[] softMax(double[] arr) {
        int length = arr.length;
        double sum = 0;
        for (int i = 0; i < length; i++) {
            sum += Math.exp(arr[i]);
        }
        double[] result = new double[length];
        for (int i = 0; i < length; i++) {
            result[i] = Math.exp(arr[i]) / sum;
        }
        return result;
    }


}
