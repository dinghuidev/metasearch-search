package org.zp.search.utils;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @Author zhangyi
 * @Date 2023年04月16日 12:03
 * @Description ElasticSearch工具
 */
@Component
@Slf4j
public class ElasticSearchUtil {

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    public SearchResponse excuteQuery(SearchSourceBuilder sourceBuilder,String[] index,String query){
        SearchRequest searchRequest = new SearchRequest(index);
        sourceBuilder.timeout(new TimeValue(500, TimeUnit.MILLISECONDS));
        long start = System.currentTimeMillis();
        searchRequest.source(sourceBuilder);
        SearchResponse response =null;
        try {
            response=restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("[ERROR] query-{} ElasticSearchUtil-excuteQuery-search error, {}", query,e.toString());
        }
        long end = System.currentTimeMillis();
        log.info("query-{} ElasticSearchUtil-excuteQuery es请求耗时结果为 {} ms",query, end - start);
        return response;
    }
}
