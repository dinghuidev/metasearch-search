package org.zp.search.utils;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class BasicUtil {

	public HashMap<String,ArrayList<String>> synons = new HashMap<>();
	public HashMap<String,ArrayList<String>> direct_synons = new HashMap<>();
	public Pattern eng_pattern= Pattern.compile("[a-zA-Z]+");
	public String regex_clean = "[^a-zA-Z0-9\u4e00-\u9fa5]";

	public double avg_idf=0.0;
	public HashMap<String,Double> idf=new HashMap<>();

	public HashSet<String> Mood_Particle_words = new HashSet<String>(){{add("吧");add("罢");add("呗");add("啵");add("啦");add("唻");add("了");add("嘞");add("哩");add("咧");add("咯");add("啰");add("喽");add("吗");add("嘛");add("嚜");add("么");add("麽");add("哪");add("呢");add("呐");add("否");add("呵");add("哈");add("不");add("兮");add("般");add("则");add("连");add("罗");add("给");add("噻");add("哉");add("呸");add("阿");add("啊");add("呃");add("欸");add("哇");add("呀");add("也");add("耶");add("哟");add("欤");add("呕");add("噢");add("呦");add("嘢");}};
	public HashSet<String> Mood_Particle_gap_words = new HashSet<String>(){{add("什么"); add("有么"); add("有吗");}};
	public Pattern digit_regex = Pattern.compile("[0-9]+");

	public ArrayList<String> languages=new ArrayList<>();

	public Pattern oneword_query_synon_pattern = Pattern.compile("^什么是(.*)",Pattern.MULTILINE);

	public HashSet<String> oneword_query_synon_suffix = new HashSet<String>(){{add("是什么意思");add("是什么");add("什么意思");add("是指什么");add("指什么");}};
	public HashSet<String> oneword_query_synon_suffix_priority1 = new HashSet<String>(){{add("是什么");add("是指什么");add("指什么");}};
	public HashSet<String> oneword_query_synon_suffix_priority2 = new HashSet<String>(){{add("是什么意思");add("什么意思");}};
	public HashSet<String> oneword_query_synon_prefix = new HashSet<String>(){{add("什么是");}};

	public Pattern suffix_prefix_match_1 = Pattern.compile("^一个[\u4e00-\u9fa5]{1,4}一个[\u4e00-\u9fa5]{1,4}$");
	public Pattern suffix_prefix_match_1_ = Pattern.compile("^一个[\u4e00-\u9fa5]{1,4}(加上|加|下面|左面|右面|上面)(一个|个)[\u4e00-\u9fa5]{1,4}$");
	public Pattern suffix_prefix_match_2 = Pattern.compile("^[\u4e00-\u9fa5]{1,4}一个[\u4e00-\u9fa5]{1,4}$");
	public Pattern suffix_prefix_match_2_ = Pattern.compile("^[\u4e00-\u9fa5]{1,4}(加上|加)一个[\u4e00-\u9fa5]{1,4}$");
	public Pattern suffix_prefix_match_3 = Pattern.compile("^(上面|下面|上|左面|右面)一个[\u4e00-\u9fa5]{1,4}(上面|下面|下|左面|右面)(一个|个)[\u4e00-\u9fa5]{1,4}$");

	public ArrayList<String> vocab_query_synon_suffix = new ArrayList<String>(){{add("是什么");add("有哪些");add("有什么");}};

	public ArrayList<String> vocab_query_symbol = new ArrayList<String>(){{add("近义词");add("反义词");add("同义词");}};

	public ArrayList<String> suffix_prefix_match_1_char=new ArrayList<String>(){{add("下面");add("左面");add("右面");add("上面");}};

	public String regex_punc_ws ="[\\s\\!\\\"\\#\\$\\%\\&\\'\\(\\)\\*\\+\\,\\-\\.\\/\\:\\;\\<\\=\\>\\?\\@\\[\\\\\\]\\^\\_\\`\\{\\|\\}\\~]";

	public void Load_Synons() throws Exception{
		Resource resource = new ClassPathResource("data/synons");
		InputStream inputStream = resource.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		String line;
		while ((line=br.readLine())!=null) {
			String[] values = line.replaceAll("(^\\h*)|(\\h*$)","").split("\t");
			if(!direct_synons.containsKey(values[0])){
				direct_synons.put(values[0],new ArrayList());
			}
			for (int i = 0; i < values.length; i++) {
				if(direct_synons.get(values[0]).contains(values[i])){
					continue;
				}
				else{
					direct_synons.get(values[0]).add(values[i]);
				}
			}
			for (int i = 0; i < values.length; i++){
				if(!synons.containsKey(values[i])){
					synons.put(values[i],new ArrayList());
				}
				for (int j = 0; j < values.length; j++){
					if(i==j){
						continue;
					}
					if(synons.get(values[i]).contains(values[j])){
						continue;
					}
					else{
						synons.get(values[i]).add(values[j]);
					}
				}
			}
			}
		br.close();
		inputStream.close();
		System.out.println("Loaded Synons Dict");
	}

	public void Load_Languages() throws Exception{
		Resource resource = new ClassPathResource("data/language.txt");
		InputStream inputStream = resource.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		String line;
		while ((line=br.readLine())!=null) {
			String[] values = line.replaceAll("(^\\h*)|(\\h*$)","").split("\n");
			if(!languages.contains(values[0])){
				languages.add(values[0]);
			}
		}
		br.close();
		inputStream.close();
		System.out.println("Loaded Language Dict");
	}

	public void Load_Idf() throws Exception{
		Resource resource = new ClassPathResource("data/idf.txt");
		InputStream inputStream = resource.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		String line;
		Double total_idf=0.0;
		Double current_idf=0.0;
		while ((line=br.readLine())!=null) {
			String[] values = line.replaceAll("(^\\h*)|(\\h*$)","").split("\t");
			current_idf=Double.parseDouble(values[1]);
			total_idf+=current_idf;
			idf.put(values[0],current_idf);
		}
		avg_idf=total_idf/idf.size();
		br.close();
		inputStream.close();
		System.out.println("Loaded IDF");
	}

	public ArrayList<String> synons_finder(String words,Integer flag){
		ArrayList<String> synons_word=new ArrayList<>();
		if(flag==1) {
			if (synons.containsKey(words)) {
				for (String w : synons.get(words)) {
					if (w.length() > 1) {
						synons_word.add(w);
					}
				}
			}
		}
		else if(flag==2){
			if (direct_synons.containsKey(words)) {
				for (String w : direct_synons.get(words)) {
					if (w.length() > 1) {
						synons_word.add(w);
					}
				}
			}
		}
		return synons_word;
	}
	public Boolean is_eng(String words){
		Matcher eng_res = eng_pattern.matcher(words);
		Boolean ret=false;
		if(eng_res.find()){
			if(eng_res.group().equals(words)){
				ret=true;
			}
		}
		return ret;
	}

	public Boolean translate_detect(String query){
		Boolean ret=false;
		for(String k:languages){
			if(query.contains(k)){
				ret=true;
				break;
			}
		}
		return ret;
	}


}
