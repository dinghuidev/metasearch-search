package org.zp.search.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.google.common.primitives.Floats;
import com.huaban.analysis.jieba.JiebaSegmenter;
import com.huaban.analysis.jieba.SegToken;
import lombok.extern.slf4j.Slf4j;
import ml.dmlc.xgboost4j.java.DMatrix;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zp.search.pojo.SegTemplate;
import org.zp.search.task.MetaAsyncTask;
import org.zp.search.utils.QueryPreProcessUtil;
import org.zp.search.utils.SimCalculateUtil;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @Author zhangyi
 * @Date 2023年04月24日 18:14
 * @Description
 */
@Service
@Slf4j
public class MetaSearchService {

    private final JiebaSegmenter jiebaSegmenter = new JiebaSegmenter();
    @Autowired
    private SimCalculateUtil simCalculateUtil;

    @Autowired
    private ElasticSearchService elasticSearchService;

    @Autowired
    private QueryPreProcessUtil queryPreProcessUtil;

    @Autowired
    private MetaAsyncTask metaAsyncTask;

    //弱相关
    private static final float WEAK_CORRELATION = -5.1f;
    //强相关
    private static final float STRONG_CORRELATION = 2.3f;

    //bertNLISim得分阈值
    private static final float BERTNLISIM_SCORE = 0.9f;
    //bertNLISim过滤阈值
    private static final float BERTNLISIM_FILTER_SCORE = 0.29f;


    public List<Map<String, Object>> search(String oriQuery, String[] index, int searchSize, int isUseSynons) {

        /*query的预处理*/
        String query = queryPreProcessUtil.queryClean(oriQuery);
        log.info("query-{} MetaSearchService-search-queryClean {}", oriQuery, query);
        SegTemplate cutWordsRes = null;
        JSONArray queryEmbeddingRes = null;
        /*获取切词，embedding，权重信息*/
        CompletableFuture<SegTemplate> cutWordsFuture = metaAsyncTask.getCutWords(query);
        CompletableFuture<JSONArray> embeddingFuture = metaAsyncTask.getEmbedding(Collections.singletonList(query));

        try {
            cutWordsRes = cutWordsFuture.get(10000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaSearchService-search-cut-words service error {}", query, e.getMessage());
        }

        try {
            queryEmbeddingRes = embeddingFuture.get(5000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaSearchService-search-get-embedding service error {}", query, e.getMessage());
        }

        if (cutWordsRes == null || queryEmbeddingRes == null || queryEmbeddingRes.isEmpty()) {
            log.error("[ERROR] query-{} MetaSearchService-search-res null error", query);
            return null;
        }
        List<Double> embeddingQuery = queryEmbeddingRes.getJSONArray(0).toJavaList(Double.class);

        /*解析切词的结果*/
        //query的关键词列表
        List<String> queryKwsList = new ArrayList<>();
        try {
            queryKwsList = cutWordsRes.getQuery_kws_lst();
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaSearchService-search-ParserQueryKwsSetError {}", query, e.getMessage());
        }

        int queryKwsListLen = queryKwsList.size();
        //query切词结果
        List<String> querySeg = new ArrayList<>();
        try {
            querySeg = cutWordsRes.getQuery_seg();
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaSearchService-search-ParserQuerySegError {}", query, e.getMessage());
        }
        //query关键词的同义词
        HashMap<String, ArrayList<String>> queryKwsSynons = new HashMap<>();
        try {
            queryKwsSynons = cutWordsRes.getQuery_kws_synons();
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaSearchService-search-ParserQueryKwsSynonsError {}", query, e.getMessage());
        }
        //query的words结果
        List<String> queryWords = new ArrayList<>();
        try {
            ArrayList<String> words = cutWordsRes.getQuery_tags_kws().get("words");
            if (null != words) {
                queryWords = JSON.parseObject(JSON.toJSONString(words),
                        new TypeReference<ArrayList<String>>() {
                        });
            }
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaSearchService-search-ParserQueryWordsError {}", query, e.getMessage());
        }
        //query的ner结果
        List<String> queryNers = new ArrayList<>();
        try {
            ArrayList<String> ner = cutWordsRes.getQuery_tags_kws().get("ner");
            if (null != ner) {
                queryNers = JSON.parseObject(JSON.toJSONString(ner),
                        new TypeReference<ArrayList<String>>() {
                        });
            }
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaSearchService-search-ParserQueryNersError {}", query, e.getMessage());
        }
        log.info("query-{} MetaSearchService-search-queryKwsList {}", query, queryKwsList);
        log.info("query-{} MetaSearchService-search-querySeg {}", query, querySeg);
        log.info("query-{} MetaSearchService-search-queryKwsSynons {}", query, queryKwsSynons);
        log.info("query-{} MetaSearchService-search-queryNers {}", query, queryNers);
        log.info("query-{} MetaSearchService-search-queryKwsListLen {}", query, queryKwsListLen);

        /*es召回数据*/
        //如果关键词大于3个,则会用到TermWeight服务
        SearchHit[] esResult = null;
        if (queryKwsListLen > 5) {
            //是否使用同义词查询
            if (isUseSynons == 1) {
                esResult = elasticSearchService.searchWithSynsTermWeight(query, index, searchSize, queryKwsSynons);
            } else {
                esResult = elasticSearchService.searchWithTermWeight(query, index, searchSize);
            }
        } else {
            //小于3个关键词直接match查询
            esResult = elasticSearchService.search(query, index, searchSize);
        }

        if (esResult == null || esResult.length == 0) {
            log.error("[ERROR] query-{} MetaSearchService-search-esResult is Null ", query);
            return new ArrayList<>();
        }
        log.info("query-{} MetaSearchService-search-es召回结果数量 {}", query, esResult.length);
        /*解析召回的es数据*/
        //title平均长度
        double avgTitlesLen = 0.0;
        int titlesAmount = 0;
        ArrayList<Map<String, Object>> resultList = new ArrayList<>();
        HashSet<String> linkSet = new HashSet<>();
        for (SearchHit hit : esResult) {
            Map<String, Object> sourceAsMap = hit.getSourceAsMap();
            //数据id
            String id = hit.getId();
            //数据索引
            String index1 = hit.getIndex();
            List<Map<String, Object>> results = JSON.parseObject(JSONObject.toJSONString(sourceAsMap.get("results")),
                    new TypeReference<List<Map<String, Object>>>() {
                    });

            for (Map<String, Object> result : results) {
                String link = result.get("link").toString();
                String title = result.get("title").toString();
                String summary = result.get("summary").toString();
                //做一些过滤
                if (StringUtils.isEmpty(title) || StringUtils.isEmpty(link))
                    continue;
                if (link.contains("mail"))
                    continue;
                if (linkSet.contains(link))
                    continue;
                linkSet.add(link);

                int titleLength = title.length();
                int summaryLength = summary.length();
                if (titleLength > 0 && titleLength < 100 && summaryLength > 0 && summaryLength < 500) {
                    avgTitlesLen = avgTitlesLen + titleLength;
                    titlesAmount += 1;
                    result.put("query", sourceAsMap.get("query"));
                    result.put("id", id);
                    result.put("index", index1);
                    resultList.add(result);
                }

            }
        }
        avgTitlesLen = avgTitlesLen / titlesAmount;
        log.info("query-{} MetaSearchService-search-es召回结果解析之后数量为 {},title平均长度为 {}", query, resultList.size(), avgTitlesLen);

        /*计算embedding*/
        //title的向量信息
        Future<JSONArray> titleEmbeddingFuture =
                metaAsyncTask.getEmbedding(resultList.stream().map(r -> r.get("title").toString()).collect(Collectors.toList()));
        //nli服务的结果
        Future<List<Double>> bertNLIFuture =
                metaAsyncTask.getBertNLI(query, resultList.stream().map(r -> r.get("title").toString()).collect(Collectors.toList()));
        //title的切词信息
        List<Future<SegTemplate>> titlesSegFuture =
                resultList.stream().map(r -> metaAsyncTask.getCutWords(r.get("title").toString())).collect(Collectors.toList());


        List<List<SegToken>> titleJiebaSeg =
                resultList.stream().map(r -> jiebaSegmenter.process(r.get("title").toString(), JiebaSegmenter.SegMode.SEARCH))
                        .collect(Collectors.toList());

        //获取future的结果
        JSONArray titleEmbedding = new JSONArray();

        try {
            titleEmbedding = titleEmbeddingFuture.get(5000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaSearchService-search-titleEmbedding service error {}", query, e.getMessage());
        }

        List<Double> bertNLI = new ArrayList<>();

        try {
            bertNLI = bertNLIFuture.get(5000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaSearchService-search-bertNLI service error {}", query, e.getMessage());
        }

        /*在result中保存相关特征数据*/
        for (int i = 0; i < resultList.size(); i++) {
            resultList.get(i).put("bertVec", null == titleEmbedding ? new ArrayList<>() : titleEmbedding.get(i));
            resultList.get(i).put("bertNLISim", null == bertNLI ? 0.0 : bertNLI.get(i));

            SegTemplate titlesSegTmp = new SegTemplate();
            try {
                titlesSegTmp = titlesSegFuture.get(i).get(10000, TimeUnit.MILLISECONDS);
            } catch (Exception e) {
                log.error("[ERROR] query-{} MetaSearchService-search-titlesSeg service error {}", query, e.getMessage());
            }

            resultList.get(i).put("titleClean", null == titlesSegTmp ? "" : titlesSegTmp.getCleaned_query());
            resultList.get(i).put("titleWords", null == titlesSegTmp ? new ArrayList<String>() : titlesSegTmp.getQuery_kws_lst());
            resultList.get(i).put("jiebaWordCut", titleJiebaSeg.get(i));
            ArrayList<String> nerTmp = null == titlesSegTmp ? new ArrayList<>() : titlesSegTmp.getQuery_tags_kws().get("ner");
            resultList.get(i).put("titleFeatures", new JSONObject() {{
                put("ner", nerTmp);
            }});
            ArrayList<String> wordsTmp = null == titlesSegTmp ? new ArrayList<>() : titlesSegTmp.getQuery_tags_kws().get("words");
            resultList.get(i).put("titleFeaturesWords", new JSONObject() {{
                put("words", wordsTmp);
            }});
            resultList.get(i).put("title_words_syn", null == titlesSegTmp ? new HashMap<String, ArrayList<String>>() : titlesSegTmp.getQuery_kws_synons());
        }

        log.info("query-{} MetaSearchService-search-在result中保存相关特征数据之后,总长度为 {}", query, resultList.size());

        /*解析保存的特征*/
        ArrayList<Map<String, Object>> newResultList = new ArrayList<>();

        for (Map<String, Object> result : resultList) {
            //title向量
            List<Double> embeddingTitle = JSON.parseObject(JSONObject.toJSONString(result.get("bertVec")),
                    new TypeReference<List<Double>>() {
                    });
            //清洗后的title
            String cleanTitle = result.get("titleClean").toString();

            //title的关键词
            List<String> titleKwsList = JSON.parseObject(JSONObject.toJSONString(result.get("titleWords")),
                    new TypeReference<List<String>>() {
                    });
            //title结巴切词结果
            List<String> titleJiebaSegWords = JSON.parseObject(JSONObject.toJSONString(result.get("jiebaWordCut")),
                    new TypeReference<List<String>>() {
                    });
            //title的ner结果
            List<String> titleNers = new ArrayList<>();
            try {
                Map<String, Object> titleFeaturesMap = JSON.parseObject(JSONObject.toJSONString(result.get("titleFeatures")),
                        new TypeReference<Map<String, Object>>() {
                        });
                List<String> titleNerTmp = JSON.parseObject(JSONObject.toJSONString(titleFeaturesMap.get("ner")),
                        new TypeReference<List<String>>() {
                        });
                if (null != titleNerTmp) {
                    titleNers = JSON.parseObject(JSON.toJSONString(titleNerTmp),
                            new TypeReference<ArrayList<String>>() {
                            });
                }
            } catch (Exception e) {
                log.error("[ERROR] query-{} MetaSearchService-search-Parse titleNers error {}", query, e.getMessage());
            }

            //title的words结果
            List<String> titleWords = new ArrayList<>();
            try {
                Map<String, Object> titleFeaturesWordsMap = JSON.parseObject(JSONObject.toJSONString(result.get("titleFeaturesWords")),
                        new TypeReference<Map<String, Object>>() {
                        });
                List<String> titleWordsTmp = JSON.parseObject(JSONObject.toJSONString(titleFeaturesWordsMap.get("words")),
                        new TypeReference<List<String>>() {
                        });
                if (null != titleWordsTmp) {
                    titleWords = JSON.parseObject(JSON.toJSONString(titleWordsTmp),
                            new TypeReference<ArrayList<String>>() {
                            });
                }
            } catch (Exception e) {
                log.error("[ERROR] query-{} MetaSearchService-search-Parse titleWords error {}", query, e.getMessage());
            }
            //title的关键词的同义词
            Map<String, ArrayList<String>> titleKeyWordsSyns = JSON.parseObject(JSONObject.toJSONString(result.get("title_words_syn")),
                    new TypeReference<Map<String, ArrayList<String>>>() {
                    });

            //放title的关键词以及关键词的同义词
            HashSet<String> titleKeyWordsSynsSet = new HashSet<>();
            for (Map.Entry<String, ArrayList<String>> titleSynonsMap : titleKeyWordsSyns.entrySet()) {
                titleKeyWordsSynsSet.add(titleSynonsMap.getKey());
                titleKeyWordsSynsSet.addAll(titleSynonsMap.getValue());
            }
            titleKeyWordsSynsSet.addAll(titleKwsList);

            //处理排序特征
            double simFeat = (double) simCalculateUtil.simFeat(queryKwsList, titleKwsList, query, cleanTitle).get(0);
            // 剪枝simFeat = 0.0
//            if (simFeat == null) {
//                simFeat = 0.0;
//            }
//            if (simFeat <= 0.0) //会卡掉不少数据，减少召回
//                continue;
            double simBert = simCalculateUtil.bertSim(embeddingQuery, embeddingTitle);
            int lenTitle = result.get("title").toString().length();
            int titleKwsListLen = titleKwsList.size();

            result.put("simBert", simBert);
            result.put("accuracySim1", simCalculateUtil.accuracySim1(query, result.get("title").toString()));
            result.put("accuracySim2", simCalculateUtil.accuracySim2(query, result.get("title").toString()));
            result.put("editDisSim", simCalculateUtil.editDistance(query, cleanTitle) * 1.0 / lenTitle);
            result.put("getLcsSim", simCalculateUtil.getLcsLen(query, cleanTitle) * 1.0 / lenTitle);
            result.put("queryLen", query.length());
            result.put("titleLen", cleanTitle.length());
            result.put("qLenDivTLen", (query.length() * 1.0 / cleanTitle.length()));
            result.put("queryKwsSetLen", queryKwsListLen);
            result.put("titleKwsSetLen", titleKwsListLen);
            result.put("queryTitleCommonKwsRate",
                    simCalculateUtil.queryTitleCommonKws(queryKwsList, titleKwsList).size() * 1.0 /
                            (queryKwsListLen + titleKwsListLen));
            result.put("queryTitleCommonWordsRate",
                    titleWords.size() > 0 ? simCalculateUtil.qMatchTKwsNum(queryWords, titleWords) * 1.0 /
                            titleWords.size() : 0.0f);
            result.put("queryTitleDiffKwsRate",
                    (queryKwsListLen - simCalculateUtil.queryTitleCommonKws(queryKwsList, titleKwsList).size()) * 1.0 /
                            (queryKwsListLen));
            result.put("bm25SimilarOfChars", simCalculateUtil.bm25SimilarOfChars(querySeg, titleJiebaSegWords, avgTitlesLen));
            result.put("bm25SimilarOfWords", simCalculateUtil.bm25SimilarOfWords(querySeg, cleanTitle, avgTitlesLen));
            result.put("queryMatchTitleNers", simCalculateUtil.qMatchTNersNum(queryNers, titleNers));
            result.put("queryMatchTitleNersWithSynons", simCalculateUtil.qMatchTNersNumWithSynons(queryNers, titleKeyWordsSynsSet));
            result.put("queryMatchTitleKws", simCalculateUtil.qMatchTKwsNum(queryKwsList, titleKwsList));
            result.put("queryMatchTitleKwsWithSynons", simCalculateUtil.qMatchTKwsNumWithSynons(queryKwsList, titleKeyWordsSynsSet));
            result.put("simFeat", simFeat);

            newResultList.add(result);

        }
        log.info("query-{} MetaSearchService-search-解析完在result中保存的相关特征数据之后,总长度为 {}", query, newResultList.size());

        if (newResultList.size() == 0) {
            log.error("[ERROR] query-{} MetaSearchService-search-newResultList is null", query);
            return new ArrayList<>();
        }

        ArrayList<Float> featureMatrix = new ArrayList<>();
        List<String> featuresSet = new ArrayList<String>() {
            {
                add("simBert");
                add("accuracySim1");
                add("accuracySim2");
                add("editDisSim");
                add("getLcsSim");
                add("queryLen");
                add("titleLen");
                add("qLenDivTLen");
                add("queryKwsSetLen");
                add("titleKwsSetLen");
                add("queryTitleCommonKwsRate");
                add("queryTitleDiffKwsRate");
                add("bm25SimilarOfChars");
                add("bm25SimilarOfWords");
                add("queryMatchTitleNers");
                add("queryMatchTitleNersWithSynons");
                add("queryMatchTitleKws");
                add("queryMatchTitleKwsWithSynons");
                add("simFeat");
            }
        };
        //featureMatrix矩阵填充
        for (Map<String, Object> ele : newResultList) {
            for (String featureName : featuresSet) {
                try {
                    String tmp = ele.get(featureName).toString();
                    featureMatrix.add(Float.valueOf(tmp));
                } catch (Exception e) {
                    log.error("[ERROR] query-{} MetaSearchService-search-featureMatrix cost error,featureName is {},value is {}", query, featureName, ele.get(featureName).toString());
                }
            }
        }

        //矩阵构造
        DMatrix dMatrix = null;
        try {
            dMatrix = new DMatrix(Floats.toArray(featureMatrix), newResultList.size(), featuresSet.size(), Float.NaN);
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaSearchService-search-ModelParamsConvertError {}", query, e.getMessage());
            return new ArrayList<>();
        }

        //模型预测结果
        float[][] predRes = null;
        try {
            predRes = simCalculateUtil.getTitlePredictor().predict(dMatrix);
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaSearchService-search-ModelPredictError {}", query, e.getMessage());
            return new ArrayList<>();
        }
        log.info("query-{} MetaSearchService-search-模型预测结果的纬度 数据量-{} 特征数量-{}", query, predRes.length, predRes[0].length);
        /*排序前的处理,得到一些中间得分*/
        for (int i = 0; i < newResultList.size(); i++) {
            Map<String, Object> resultTmp = newResultList.get(i);

            //模型预测得分
            float predResEle = predRes[i][0];

            float simBert = (float) Double.parseDouble(resultTmp.get("simBert").toString());
            float addScore = 0.0f;
            if (predResEle < 1) {
                addScore = (simBert - 0.8f) * 3.5f * queryKwsList.size();
            }

            newResultList.get(i).put("modelScore", predResEle);
            newResultList.get(i).put("titleAddScore", addScore);

            float titleQueryTotalMatch = 0.0f;

            //queyr和title的公共词占title词的比例，太小的话说明title的冗余信息较多，应降权
            double queryTitleCommonWordsRate = (double) resultTmp.get("queryTitleCommonWordsRate");

            if (query.length() >= 4 && ((resultTmp.get("titleClean").toString().contains(query)
                    || (int) resultTmp.get("queryMatchTitleKwsWithSynons") == queryKwsList.size()) && queryTitleCommonWordsRate > 0.35)) {
                titleQueryTotalMatch = 1.0f + (float) queryTitleCommonWordsRate;
                newResultList.get(i).put("titleQueryTotalMatch", titleQueryTotalMatch);
            } else {
                titleQueryTotalMatch = ((int) resultTmp.get("queryMatchTitleKwsWithSynons")) * 1.0f / queryKwsList.size() + (float) queryTitleCommonWordsRate;
                newResultList.get(i).put("titleQueryTotalMatch", titleQueryTotalMatch);
            }


            float titleKwsSetLenTmp = ((Integer) resultTmp.get("titleKwsSetLen")).floatValue();
            float addScore2Tmp = (1.5f - (titleKwsSetLenTmp - 7) * (titleKwsSetLenTmp - 7) * 0.222f);
            float addScore2 = 0.0f;
            if (addScore2Tmp > 0 && predResEle < 1) {
                addScore2 = addScore2Tmp;
            }
            newResultList.get(i).put("titleAddScore2", addScore2);
            newResultList.get(i).put("titleRankPred", predResEle + titleQueryTotalMatch * 4);

        }
        log.info("query-{} MetaSearchService-search-过滤前的result数量-{}", query, newResultList.size());
        /*根据得分过滤一批数据*/
        List<Map<String, Object>> resultFinalTmp = newResultList.stream()
                .filter(r -> Double.parseDouble(r.get("simFeat").toString()) > 0.5f ||Double.parseDouble(r.get("bertNLISim").toString()) > 0.85f || Float.parseFloat(r.get("modelScore").toString()) > WEAK_CORRELATION && Double.parseDouble(r.get("bertNLISim").toString()) > BERTNLISIM_FILTER_SCORE)
                .collect(Collectors.toList());
//        List<Map<String, Object>> resultFinalTmp = newResultList;
        boolean flag = false;
        if (resultFinalTmp.size() < newResultList.size()) {
            flag = true;
        }
        log.info("query-{} MetaSearchService-search-过滤后的result数量-{}", query, resultFinalTmp.size());

        float maxTitleRankScore = Float.MIN_VALUE;
        float minTitleRankScore = Float.MAX_VALUE;

        for (Map<String, Object> ele : resultFinalTmp) {
            maxTitleRankScore = Math.max(maxTitleRankScore, (float) ele.get("titleRankPred"));
            minTitleRankScore = Math.min(minTitleRankScore, (float) ele.get("titleRankPred"));
        }

        /*算最终的排序得分*/
        ArrayList<Map<String, Object>> resultFinal = new ArrayList<>();

        for (Map<String, Object> ele : resultFinalTmp) {

            HashMap<String, Object> map = new HashMap<>();
//            float finalBertScore = (float) Double.parseDouble(ele.get("simBert").toString());
            float finalBertScore = (float) Double.parseDouble(ele.get("bertNLISim").toString());

            float finalTitleRankScore = 0.0f;
            if (maxTitleRankScore > minTitleRankScore) {
                finalTitleRankScore = ((float) ele.get("titleRankPred") - minTitleRankScore) / (maxTitleRankScore - minTitleRankScore);
            }

            ele.put("finalTitleRankScore", finalTitleRankScore);
            ele.put("finalScore", finalBertScore * 2.0f + finalTitleRankScore);
            ele.put("finalScoreList", new ArrayList<Object>() {{
                add(finalBertScore);
                add(ele.get("finalTitleRankScore"));
            }});
            //强相关整体往前排
            if ((float) ele.get("modelScore") > STRONG_CORRELATION || finalBertScore > BERTNLISIM_SCORE) {
                ele.put("finalScore", (float) ele.get("finalScore") + 3.0f * finalBertScore);
            }

            map.put("filter", flag);
            map.put("query", ele.get("query"));
            map.put("title", ele.get("title"));
            map.put("title_em", ele.get("title_em"));
            map.put("summary", ele.get("summary"));
            map.put("summary_em", ele.get("summary_em"));
            map.put("link", ele.get("link"));
            map.put("link2", ele.get("link2"));
            map.put("aladdin", ele.get("aladdin"));
            map.put("srcid", ele.get("srcid"));
            map.put("rank", ele.get("rank"));
            map.put("tail", ele.get("tail"));
            map.put("tpl", ele.get("tpl"));
            map.put("titleClean", ele.get("titleClean"));
            map.put("simBert", ele.get("simBert"));
            map.put("simFeat", ele.get("simFeat"));
            map.put("titleAddScore", ele.get("titleAddScore"));
            map.put("titleAddScore2", ele.get("titleAddScore2"));
            map.put("titleRankPred", ele.get("titleRankPred"));
            map.put("titleQueryTotalMatch", ele.get("titleQueryTotalMatch"));
            map.put("finalScoreList", ele.get("finalScoreList"));
            map.put("modelScore", ele.get("modelScore"));
            map.put("bertNLISim", ele.get("bertNLISim"));
            map.put("finalScore", ele.get("finalScore"));
            map.put("id", ele.get("id"));
            map.put("index", ele.get("index"));
            map.put("queryTitleCommonWordsRate", ele.get("queryTitleCommonWordsRate"));
            map.put("editDisSim", ele.get("editDisSim"));
            map.put("getLcsSim", ele.get("getLcsSim"));
            resultFinal.add(map);
        }

        Comparator<Map<String, Object>> scoreComparatorFloat = Comparator.comparingDouble(c -> Double.parseDouble(c.get("finalScore").toString()));

        resultFinal.sort(scoreComparatorFloat.reversed());

        return resultFinal.size() > 50 ? resultFinal.subList(0, 50) : resultFinal;
//        return resultFinal;
    }

}
