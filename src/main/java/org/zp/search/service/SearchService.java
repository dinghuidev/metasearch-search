package org.zp.search.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.huaban.analysis.jieba.JiebaSegmenter;
import com.huaban.analysis.jieba.SegToken;
import com.huaban.analysis.jieba.WordDictionary;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.*;
import org.elasticsearch.index.search.MatchQuery;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.zp.search.task.MetaAsyncTask;
import org.zp.search.utils.ElasticSearchUtil;
import org.zp.search.utils.TextUtil;

import java.io.*;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author zhangyi
 * 初始版本service，已弃用
 */
@Service
@Slf4j
public class SearchService {

    private static final Map<String, List<String>> synonym = new HashMap<>();

    static {
//        Resource resource = new ClassPathResource("dic/sougou.dict");
//        String tempPath = System.getProperty("java.io.tmpdir") + "/sougou.dict";
//
//        File f = new File(tempPath);
//        try {
//            IOUtils.copy(resource.getInputStream(), new FileOutputStream(f));
//            WordDictionary.getInstance().loadUserDict(f.toPath());
//        } catch (IOException e) {
//            log.error("[ERROR] MetaSearchService-static load resource error {}", e.getMessage());
//        }

        //加载同义词典

        Resource classPathResource = new ClassPathResource("data/synons");
        InputStream inputStream = null;
        try {
            inputStream = classPathResource.getInputStream();
            String line = null;
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = br.readLine()) != null) {
                String[] words = line.split("\t");
                if (words.length == 2) {
                    String word = words[0];
                    String syns = words[1];
                    if (synonym.containsKey(word)) {
                        List<String> synStrings = synonym.get(word);
                        synStrings.add(syns);
                        synonym.put(word, synStrings);
                    } else {
                        ArrayList<String> objects = new ArrayList<>();
                        objects.add(syns);
                        synonym.put(word, objects);
                    }
                }
            }
            System.out.println("load syns done " + synonym.get("帝都"));
        } catch (IOException e) {
            log.error("[ERROR] MetaSearchService-static load syns dict error {}", e.getMessage());
        }
    }

    private final JiebaSegmenter jiebaSegmenter = new JiebaSegmenter();
    @Autowired
    private ElasticSearchUtil elasticsearchutil;

    @Autowired
    private MetaAsyncTask metaAsyncTask;


    /**
     * @param query
     * @param index
     * @param searchSize
     * @return java.util.ArrayList<java.util.Map < java.lang.String, java.lang.Object>>
     * @author zhangyi
     * @date 2023/4/16 17:04
     */
    public List<Map<String, Object>> search(String query, String[] index, int searchSize) {

        CompletableFuture<List<Float>> termWeightFuture = metaAsyncTask.getTermWeight(query);
        List<Float> termWeight = null;
        try {
            termWeight = termWeightFuture.get(5000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaSearchService-search-Term-Weight service error {}", query, e);
        }

        log.info("query-{} MetaSearchService-search-请求term权重的结果：{}", query, termWeight);
        if (termWeight != null && termWeight.size() > 0) {

            //es参数
            float cutOff = 0.1f;
            //当使用tie_breaker时，best_field会根据tie_breaker定义的值的大小，决定除最佳匹配字段得分以外其他字段的得分
            float tieBreaker = 0.4f;
            //must查询的权重阈值
            float maxWeightScore = 0.88f;
            String miniMatch = "40%";

            List<SegToken> wordList = jiebaSegmenter.process(query, JiebaSegmenter.SegMode.SEARCH);

            log.info("query-{} MetaSearchService-search-jieba切词的结果：{}", query, wordList);

            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

            BoolQueryBuilder Q_ = QueryBuilders.boolQuery();
            //内部嵌套的bool查询
            BoolQueryBuilder Q_small = QueryBuilders.boolQuery();

            for (SegToken segToken : wordList) {
                String word = segToken.word;

                if (StringUtils.isNotEmpty(word)) {

                    int startOffset = segToken.startOffset;
                    int endOffset = segToken.endOffset;

                    float maxWeight = 0.0f;

                    for (int j = startOffset; j < endOffset; j++) {
                        maxWeight = Math.max(maxWeight, termWeight.get(j));
                    }

                    MultiMatchQueryBuilder multiMatchQueryBuilder = QueryBuilders.multiMatchQuery(word)
                            .field("query")
                            //查找匹配任意field的文档，但是文档的得分来自最匹配的那个字段
                            .type(MultiMatchQueryBuilder.Type.BEST_FIELDS)
                            .tieBreaker(tieBreaker)
                            .operator(Operator.OR)
                            .slop(0)
                            .prefixLength(0)
                            .maxExpansions(50)
                            .minimumShouldMatch(miniMatch)
                            .zeroTermsQuery(MatchQuery.ZeroTermsQuery.NONE)
                            .autoGenerateSynonymsPhraseQuery(true)
                            .fuzzyTranspositions(true)
                            .cutoffFrequency(cutOff)
                            .boost(maxWeight);
                    if (maxWeight > maxWeightScore) {
                        Q_small.should(multiMatchQueryBuilder);
                    } else {
                        Q_.should(multiMatchQueryBuilder);
                    }
                }
            }
            //2<2 当should分支总数大于2时，最少满足2个should分支
            Q_.must(Q_small.minimumShouldMatch("2<2"));
            searchSourceBuilder.query(Q_);
            log.info("query-{} MetaSearchService-serach-Query Builders:{}", query, searchSourceBuilder.toString());
            searchSourceBuilder.terminateAfter(1000);
            searchSourceBuilder.size(searchSize);

            SearchResponse response = elasticsearchutil.excuteQuery(searchSourceBuilder, index, query);
            if (response != null) {
                List<Map<String, Object>> responseResult = responseProcess(response, query);
                return responseResult;
            }
        }
        return null;
    }


    /**
     * 带同义词典的查询方法
     *
     * @param query
     * @param index
     * @param searchSize
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @author zhangyi
     * @date 2023/4/20 14:35
     */
    public List<Map<String, Object>> searchWithSyns(String query, String[] index, int searchSize) {

        CompletableFuture<List<Float>> termWeightFuture = metaAsyncTask.getTermWeight(query);
        List<Float> termWeight = null;
        try {
            termWeight = termWeightFuture.get(5000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaSearchService-searchWithSyns-Term-Weight service error {}", query, e);
            return null;
        }

//        log.info("query-{} MetaSearchService-searchWithSyns-请求term权重的结果：{}", query, termWeight);
        if (termWeight != null && termWeight.size() > 0) {

            //es参数
            float cutOff = 0.1f;
            //当使用tie_breaker时，best_field会根据tie_breaker定义的值的大小，决定除最佳匹配字段得分以外其他字段的得分
            float tieBreaker = 0.4f;
            //must查询的权重阈值
            float maxWeightScore = 0.88f;
            String miniMatch = "40%";

            List<SegToken> wordList = jiebaSegmenter.process(query, JiebaSegmenter.SegMode.SEARCH);

            log.info("query-{} MetaSearchService-searchWithSyns-jieba切词的结果：{}", query, wordList);

            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

            BoolQueryBuilder Q_ = QueryBuilders.boolQuery();
            //内部嵌套的bool查询
            BoolQueryBuilder Q_1 = QueryBuilders.boolQuery();

            HashMap<String, Object> weightMap = new HashMap<>();
            for (SegToken segToken : wordList) {
                String word = segToken.word;
                if (StringUtils.isEmpty(word)) {
                    continue;
                }

                int startOffset = segToken.startOffset;
                int endOffset = segToken.endOffset;

                float maxWeight = 0.0f;

                for (int j = startOffset; j < endOffset; j++) {
                    maxWeight = Math.max(maxWeight, termWeight.get(j));
                }
                //存储切完的词对应的权重
                weightMap.put(word, maxWeight);

                List<String> syns = synonym.get(word) == null ? new LinkedList() : synonym.get(word);

//                log.info("query-{} MetaSearchService-serach-filter-syns:{}", query, syns);
                if (!syns.contains(word)) {
                    syns.add(0, word);
                }

                BoolQueryBuilder Q_2 = QueryBuilders.boolQuery();
                BoolQueryBuilder Q_3 = QueryBuilders.boolQuery();

                for (String synWord : syns) {

                    if (synWord.matches("[a-zA-Z]+") && !word.equals(synWord))
                        continue;
                    if ((word.contains(synWord) || synWord.contains(word)) && !word.equals(synWord))
                        continue;
                    //同义词长度大于多少就不做查询
                    if (!word.equals(synWord) && synWord.length() >= 9) {
                        continue;
                    }

                    MultiMatchQueryBuilder multiMatchQueryBuilder = QueryBuilders.multiMatchQuery(synWord)
                            .field("query")
                            //查找匹配任意field的文档，但是文档的得分来自最匹配的那个字段
                            .type(MultiMatchQueryBuilder.Type.BEST_FIELDS)
                            .tieBreaker(tieBreaker)
                            .operator(Operator.OR)
                            .slop(0)
                            .prefixLength(0)
                            .maxExpansions(50)
                            .minimumShouldMatch(miniMatch)
                            .zeroTermsQuery(MatchQuery.ZeroTermsQuery.NONE)
                            .autoGenerateSynonymsPhraseQuery(true)
                            .fuzzyTranspositions(true)
                            .cutoffFrequency(cutOff)
                            .boost(maxWeight);

                    if (maxWeight > maxWeightScore) {
                        Q_2.should(multiMatchQueryBuilder);
                        Q_3.should(multiMatchQueryBuilder);
                    } else {
                        Q_.should(multiMatchQueryBuilder);
                    }
                }

                if (Q_2.hasClauses()) {
                    //Q_3为了同义词列表只匹配一个，不能同时匹配2个及以上
                    Q_3.mustNot(Q_2.minimumShouldMatch(2));
                    Q_1.should(Q_3.minimumShouldMatch(1));
//                    Q_1.should(Q_2.minimumShouldMatch(1));
                }

            }
            //2<2 当should分支总数大于2时，最少满足2个should分支
            Q_.must(Q_1.minimumShouldMatch("2<2"));
            searchSourceBuilder.query(Q_);
            log.info("query-{} MetaSearchService-serach-filter-Query Builders:{}", query, searchSourceBuilder.toString());
            log.info("query-{} MetaSearchService-searchWithSyns-filter-weightMap:{}", query, weightMap);
            searchSourceBuilder.terminateAfter(1000);
            searchSourceBuilder.size(searchSize);

            SearchResponse response = elasticsearchutil.excuteQuery(searchSourceBuilder, index, query);
            if (response != null) {
                List<Map<String, Object>> responseResult = responseProcess(response, query);
                return responseResult;
            }
        }
        return null;
    }

    /**
     * 返回结果处理，带同义词
     *
     * @param response
     * @return java.util.ArrayList<java.util.Map < java.lang.String, java.lang.Object>>
     * @author zhangyi
     * @date 2023/4/16 17:03
     */
    public List<Map<String, Object>> responseProcessWithSyns(SearchResponse response, String query, Map<String, Object> weightMap) {
        SearchHit[] responseHits = response.getHits().getHits();
        if (responseHits.length == 0)
            return new ArrayList<>();

        //权重大于synsChangeWeight的词才会做同义词替换并算相似度
        float synsChangeWeight = 0.6f;
        ArrayList<String> querys = new ArrayList<>();
        ArrayList<String> titles = new ArrayList<>();
        HashMap<String, Integer> indexMap = new HashMap<>();
        ArrayList<Map<String, Object>> resultList = new ArrayList<>();
        querys.add(query);

        for (SearchHit hit : responseHits) {
            Map<String, Object> sourceAsMap = hit.getSourceAsMap();

            List<Map<String, Object>> results = JSON.parseObject(JSONObject.toJSONString(sourceAsMap.get("results")),
                    new TypeReference<List<Map<String, Object>>>() {
                    });

            for (Map<String, Object> result : results) {
                if (StringUtils.isEmpty(result.get("title").toString()))
                    continue;
                result.put("query", sourceAsMap.get("query"));
                String title = result.get("title").toString();
                String newQuery = query;
                //{推荐=0.6310049, 景点=0.9548401, 帝都=0.9455558}
                for (Map.Entry<String, Object> entry : weightMap.entrySet()) {
                    String word = entry.getKey();
                    float weight = Float.parseFloat(entry.getValue().toString());
                    List<String> wordSyns = synonym.get(word);
                    if (weight > synsChangeWeight && wordSyns != null) {
                        for (String wordSyn : wordSyns) {
                            if (word.equals(wordSyn))
                                continue;
                            if (title.contains(wordSyn)) {
                                newQuery = newQuery.replaceAll(word, wordSyn);
                                break;
                            }
                        }
                    }
                }
                if (!newQuery.equals(query)) {
                    if (!querys.contains(newQuery)) {
                        querys.add(newQuery);
                    }
                    indexMap.put(title, querys.indexOf(newQuery));
                }
                if (!titles.contains(title)) {
                    titles.add(title);
                }
                resultList.add(result);
            }

//            resultList.addAll(results);
        }
//        log.info("query-{} MetaSearchService-responseProcess-synonym-querys:{}", query, querys);
//        log.info("query-{} MetaSearchService-responseProcess-synonym-titles:{}", query, titles);
//        log.info("query-{} MetaSearchService-responseProcess-synonym-indexMap:{}", query, JSON.toJSONString(indexMap));
        List<Map<String, Object>> resList = postSortFilterWithSyns(resultList, querys, titles, indexMap);

        return resList;
    }

    /**
     * @param response
     * @param query
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @author zhangyi
     * @date 2023/4/20 17:01
     */
    public List<Map<String, Object>> responseProcess(SearchResponse response, String query) {
//        ArrayList<Map<String, Object>> resList = new ArrayList<>();
        ArrayList<String> querys = new ArrayList<>();
        ArrayList<Map<String, Object>> resultList = new ArrayList<>();
        querys.add(query);
        for (SearchHit hit : response.getHits().getHits()) {
            Map<String, Object> sourceAsMap = hit.getSourceAsMap();

            List<Map<String, Object>> results = JSON.parseObject(JSONObject.toJSONString(sourceAsMap.get("results")),
                    new TypeReference<List<Map<String, Object>>>() {
                    });

            for (Map<String, Object> result : results) {
                if (StringUtils.isNotEmpty(result.get("title").toString())){
                    result.put("query", sourceAsMap.get("query"));
                    querys.add(result.get("title").toString());
                    resultList.add(result);
                }
            }
//            resultList.addAll(results);
        }
        List<Map<String, Object>> resList= new ArrayList<>();
        if (querys.size() > 1){
            resList = postSortFilter(resultList, querys);
        }

        return resList;
    }

    private List<Map<String, Object>> postSortFilter(List<Map<String, Object>> resultList, List<String> querys) {
        float maxFilterScore = 0.7f;
        ArrayList<Map<String, Object>> newRes = new ArrayList<>();

        String query = querys.get(0);
        CompletableFuture<JSONArray> embeddingFuture = metaAsyncTask.getEmbedding(querys);
        JSONArray embeddings = null;
        try {
            embeddings = embeddingFuture.get(5000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaSearchService-postSortFilter-get-embedding service error {}", query, e.getMessage());
            return null;
        }

        log.info("query-{} MetaSearchService-postSortFilter {},{}", query, embeddings.size(),querys.size());
        if (null != embeddings && !embeddings.isEmpty() && embeddings.size() == querys.size()) {
            List<Float> queryEmbedding =embeddings.getJSONArray(0).toJavaList(Float.class);

            for (int i = 0; i < resultList.size(); i++) {
                Map<String, Object> objectMap = resultList.get(i);
                List<Float> docTitleEmbedding = embeddings.getJSONArray(i+1).toJavaList(Float.class);

                double sim = TextUtil.similarity(queryEmbedding, docTitleEmbedding);

                objectMap.put("sim", sim);
                newRes.add(objectMap);
            }

            List<Map<String, Object>> newResDistinct = TextUtil.distinctListMapByKeys(newRes, Collections.singletonList("link"));

            if (!CollectionUtils.isEmpty(newResDistinct)) {

                newResDistinct.sort(new Comparator<Map<String, Object>>() {
                    @Override
                    public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                        float sim1 = Float.parseFloat(o1.get("sim").toString());
                        float sim2 = Float.parseFloat(o2.get("sim").toString());
                        return Float.compare(sim2, sim1);
                    }
                });
                List<Map<String, Object>> newResFilter = newResDistinct.stream().filter(r -> Float.parseFloat(r.get("sim").toString()) > maxFilterScore).collect(Collectors.toList());
//                List<Map<String, Object>> newResFilter = newResDistinct;

                if (newResFilter.size() > 50) {
                    return newResFilter.subList(0, 50);
                } else {
                    return newResFilter;
                }
            }
        }

        log.error("[ERROR] query-{} MetaSearchService-postSortFilterWithSyns-get-embedding data error", query);
        return null;
    }


    /**
     * 带同义词逻辑，去重逻辑
     *
     * @param resultList
     * @param querys
     * @param titles
     * @param indexMap
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @author zhangyi
     * @date 2023/4/20 18:10
     */
    public List<Map<String, Object>> postSortFilterWithSyns(List<Map<String, Object>> resultList, List<String> querys, List<String> titles, Map<String, Integer> indexMap) {
        float maxFilterScore = 0.7f;

        ArrayList<Map<String, Object>> newRes = new ArrayList<>();
        String query = querys.get(0);
        //query和title放一起丢到embedding服务
        ArrayList<String> allQuerys = new ArrayList<>();
        allQuerys.addAll(querys);
        allQuerys.addAll(titles);

        CompletableFuture<JSONArray> embeddingFuture = metaAsyncTask.getEmbedding(allQuerys);
        JSONArray embeddings = null;
        try {
            embeddings = embeddingFuture.get(5000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            log.error("[ERROR] query-{} MetaSearchService-postSortFilterWithSyns-get-embedding service error {}", query, e.getMessage());
            return null;
        }

        if (null != embeddings && !embeddings.isEmpty() && embeddings.size() == allQuerys.size()) {
            List<Float> queryEmbedding = null;

            for (Map<String, Object> objectMap : resultList) {

                String title = objectMap.get("title").toString();
                Integer titleQueryIndex = indexMap.get(title);
                int titleIndex = titles.indexOf(title);
                //获取title对应的embedding
                List<Float> docTitleEmbedding = embeddings.getJSONArray(titleIndex + querys.size()).toJavaList(Float.class);

                if (titleQueryIndex != null) {
                    //获取同义词替换后的query embedding
                    queryEmbedding = embeddings.getJSONArray(titleQueryIndex).toJavaList(Float.class);
                } else {
                    //没有同义词的情况下，用原query的embedding
                    queryEmbedding = embeddings.getJSONArray(0).toJavaList(Float.class);
                }

                double sim = TextUtil.similarity(queryEmbedding, docTitleEmbedding);
//                float editDistance = TextUtil.getSimilarityRatio(query, querys.get(i + 1));

                objectMap.put("sim", sim);
                newRes.add(objectMap);
            }
            List<Map<String, Object>> newResDistinct = TextUtil.distinctListMapByKeys(newRes, Collections.singletonList("link"));
            if (!CollectionUtils.isEmpty(newResDistinct)) {

                newResDistinct.sort(new Comparator<Map<String, Object>>() {
                    @Override
                    public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                        float sim1 = Float.parseFloat(o1.get("sim").toString());
                        float sim2 = Float.parseFloat(o2.get("sim").toString());
                        return Float.compare(sim2, sim1);
                    }
                });
                List<Map<String, Object>> newResFilter = newResDistinct.stream().filter(r -> Float.parseFloat(r.get("sim").toString()) > maxFilterScore).collect(Collectors.toList());
//                List<Map<String, Object>> newResFilter = newResDistinct;

                if (newResFilter.size() > 50) {
                    return newResFilter.subList(0, 50);
                } else {
                    return newResFilter;
                }
            }

        }
        log.error("[ERROR] query-{} MetaSearchService-postSortFilterWithSyns-get-embedding data error {}", query, embeddings);
        return null;
    }


}
