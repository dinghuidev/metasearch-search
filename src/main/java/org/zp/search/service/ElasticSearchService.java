package org.zp.search.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.huaban.analysis.jieba.JiebaSegmenter;
import com.huaban.analysis.jieba.SegToken;
import com.huaban.analysis.jieba.WordDictionary;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.*;
import org.elasticsearch.index.search.MatchQuery;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.zp.search.task.MetaAsyncTask;
import org.zp.search.utils.ElasticSearchUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * @Author zhangyi
 * @Date 2023年04月24日 19:39
 * @Description
 */
@Service
@Slf4j
public class ElasticSearchService {

    private final JiebaSegmenter jiebaSegmenter = new JiebaSegmenter();
    @Autowired
    private ElasticSearchUtil elasticsearchutil;

    @Autowired
    private MetaAsyncTask metaAsyncTask;

    static {
        Resource resource = new ClassPathResource("dic/sougou.dict");
        String tempPath = System.getProperty("java.io.tmpdir") + "/sougou.dict";

        File f = new File(tempPath);
        try {
            IOUtils.copy(resource.getInputStream(), new FileOutputStream(f));
            WordDictionary.getInstance().loadUserDict(f.toPath());

        } catch (IOException e) {
            log.error("[ERROR] ElasticSearchService-static load sougou.dict error {}", e.getMessage());
        }
    }

    /**
     * 带同义词查询，带权重
     * @author zhangyi
     * @date 2023/4/24 20:10
     * @param query
     * @param index
     * @param searchSize
     * @param synonym
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    public SearchHit[] searchWithSynsTermWeight(String query, String[] index, int searchSize, Map<String, ArrayList<String>> synonym) {

        CompletableFuture<List<Float>> termWeightFuture = metaAsyncTask.getTermWeight(query);
        List<Float> termWeight = null;
        try {
            termWeight = termWeightFuture.get(5000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            log.error("[ERROR] query-{} ElasticSearchService-searchWithSyns-Term-Weight service error {}", query, e);
            return null;
        }

//        log.info("query-{} MetaSearchService-searchWithSyns-请求term权重的结果：{}", query, termWeight);
        if (termWeight != null && termWeight.size() > 0) {

            //es参数
            float cutOff = 0.1f;
            //当使用tie_breaker时，best_field会根据tie_breaker定义的值的大小，决定除最佳匹配字段得分以外其他字段的得分
            float tieBreaker = 0.4f;
            //must查询的权重阈值
            float maxWeightScore = 0.85f;
            String miniMatch = "40%";

            List<SegToken> wordList = jiebaSegmenter.process(query, JiebaSegmenter.SegMode.SEARCH);

            log.info("query-{} MetaSearchService-searchWithSyns-jieba切词的结果：{}", query, wordList);

            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

            BoolQueryBuilder Q_ = QueryBuilders.boolQuery();
            //内部嵌套的bool查询
            BoolQueryBuilder Q_1 = QueryBuilders.boolQuery();

            HashMap<Object, Object> map = new HashMap<>();
            for (SegToken segToken : wordList) {
                String word = segToken.word;
                if (StringUtils.isEmpty(word)) {
                    continue;
                }

                int startOffset = segToken.startOffset;
                int endOffset = segToken.endOffset;

                float maxWeight = 0.0f;

                for (int j = startOffset; j < endOffset; j++) {
                    maxWeight = Math.max(maxWeight, termWeight.get(j));
                }
                map.put(word,maxWeight);
                List<String> syns = synonym.get(word) == null ? new LinkedList() : synonym.get(word);

//                log.info("query-{} MetaSearchService-serach-filter-syns:{}", query, syns);
                if (!syns.contains(word)) {
                    syns.add(0, word);
                }

                BoolQueryBuilder Q_2 = QueryBuilders.boolQuery();
                BoolQueryBuilder Q_3 = QueryBuilders.boolQuery();

                for (String synWord : syns) {

                    if (synWord.matches("[a-zA-Z]+") && !word.equals(synWord))
                        continue;
                    if ((word.contains(synWord) || synWord.contains(word)) && !word.equals(synWord))
                        continue;
                    //同义词长度大于多少就不做查询
                    if (!word.equals(synWord) && synWord.length() >= 9) {
                        continue;
                    }

                    MultiMatchQueryBuilder multiMatchQueryBuilder = QueryBuilders.multiMatchQuery(synWord)
                            .field("query")
                            //查找匹配任意field的文档，但是文档的得分来自最匹配的那个字段
                            .type(MultiMatchQueryBuilder.Type.BEST_FIELDS)
                            .tieBreaker(tieBreaker)
                            .operator(Operator.OR)
                            .slop(0)
                            .prefixLength(0)
                            .maxExpansions(50)
                            .minimumShouldMatch(miniMatch)
                            .zeroTermsQuery(MatchQuery.ZeroTermsQuery.NONE)
                            .autoGenerateSynonymsPhraseQuery(true)
                            .fuzzyTranspositions(true)
                            .cutoffFrequency(cutOff)
                            .boost(maxWeight);

                    if (maxWeight > maxWeightScore) {
                        Q_2.should(multiMatchQueryBuilder);
                        Q_3.should(multiMatchQueryBuilder);
                    } else {
                        Q_.should(multiMatchQueryBuilder);
                    }
                }

                if (Q_2.hasClauses()) {
                    //Q_3为了同义词列表只匹配一个，不能同时匹配2个及以上
                    Q_3.mustNot(Q_2.minimumShouldMatch(2));
                    Q_1.should(Q_3.minimumShouldMatch(1));
                }

            }
            //2<2 当should分支总数大于2时，最少满足2个should分支
            Q_.must(Q_1.minimumShouldMatch("2<2"));
            searchSourceBuilder.query(Q_);
            log.info("query-{} MetaSearchService-serach-filter-Query Builders:{}", query, searchSourceBuilder);
//            log.info("query-{} MetaSearchService-serach-filter-words term weight:{}", query, map);
            searchSourceBuilder.size(searchSize);
//            searchSourceBuilder.terminateAfter(1000000);
            SearchResponse response = elasticsearchutil.excuteQuery(searchSourceBuilder, index, query);
            if (response != null) {
//                List<Map<String, Object>> responseResult = responseProcess(response, query);
                return response.getHits().getHits();
            }
        }
        return null;
    }

    public SearchHit[] searchWithTermWeight(String query, String[] index, int searchSize) {

        CompletableFuture<List<Float>> termWeightFuture = metaAsyncTask.getTermWeight(query);
        List<Float> termWeight = null;
        try {
            termWeight = termWeightFuture.get(5000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            log.error("[ERROR] query-{} ElasticSearchService-search-Term-Weight service error {}", query, e);
        }

        log.info("query-{} MetaSearchService-search-请求term权重的结果：{}", query, termWeight);
        if (termWeight != null && termWeight.size() > 0) {

            //es参数
            float cutOff = 0.1f;
            //当使用tie_breaker时，best_field会根据tie_breaker定义的值的大小，决定除最佳匹配字段得分以外其他字段的得分
            float tieBreaker = 0.4f;
            //must查询的权重阈值
            float maxWeightScore = 0.85f;
            String miniMatch = "40%";

            List<SegToken> wordList = jiebaSegmenter.process(query, JiebaSegmenter.SegMode.SEARCH);

            log.info("query-{} MetaSearchService-search-jieba切词的结果：{}", query, wordList);

            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

            BoolQueryBuilder Q_ = QueryBuilders.boolQuery();
            //内部嵌套的bool查询
            BoolQueryBuilder Q_small = QueryBuilders.boolQuery();

            for (SegToken segToken : wordList) {
                String word = segToken.word;

                if (StringUtils.isNotEmpty(word)) {

                    int startOffset = segToken.startOffset;
                    int endOffset = segToken.endOffset;

                    float maxWeight = 0.0f;

                    for (int j = startOffset; j < endOffset; j++) {
                        maxWeight = Math.max(maxWeight, termWeight.get(j));
                    }

                    MultiMatchQueryBuilder multiMatchQueryBuilder = QueryBuilders.multiMatchQuery(word)
                            .field("query")
                            //查找匹配任意field的文档，但是文档的得分来自最匹配的那个字段
                            .type(MultiMatchQueryBuilder.Type.BEST_FIELDS)
                            .tieBreaker(tieBreaker)
                            .operator(Operator.OR)
                            .slop(0)
                            .prefixLength(0)
                            .maxExpansions(50)
                            .minimumShouldMatch(miniMatch)
                            .zeroTermsQuery(MatchQuery.ZeroTermsQuery.NONE)
                            .autoGenerateSynonymsPhraseQuery(true)
                            .fuzzyTranspositions(true)
                            .cutoffFrequency(cutOff)
                            .boost(maxWeight);
                    if (maxWeight > maxWeightScore) {
                        Q_small.should(multiMatchQueryBuilder);
                    } else {
                        Q_.should(multiMatchQueryBuilder);
                    }
                }
            }
            //2<2 当should分支总数大于2时，最少满足2个should分支
            Q_.must(Q_small.minimumShouldMatch("2<2"));
            searchSourceBuilder.query(Q_);
            log.info("query-{} MetaSearchService-serach-Query Builders:{}", query, searchSourceBuilder.toString());
            searchSourceBuilder.size(searchSize);
//            searchSourceBuilder.terminateAfter(1000000);
            SearchResponse response = elasticsearchutil.excuteQuery(searchSourceBuilder, index, query);
            if (response != null) {
//                List<Map<String, Object>> responseResult = responseProcess(response, query);
                return response.getHits().getHits();
            }
        }
        return null;
    }

    public SearchHit[] search(String query, String[] index, int searchSize){

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        float cutOff = 0.1f;
        QueryBuilder queryBuilder = QueryBuilders.matchQuery("query", query);
//                .cutoffFrequency(cutOff)
//                .minimumShouldMatch("60%");

        searchSourceBuilder.query(queryBuilder);
        searchSourceBuilder.size(searchSize);
//        log.info("query-{} MetaSearchService-serach-Query Builders:{}", query, searchSourceBuilder.toString());
        SearchResponse response = elasticsearchutil.excuteQuery(searchSourceBuilder, index, query);
        if (response != null) {
//            List<Map<String, Object>> responseResult = responseProcess(response, query);
            return response.getHits().getHits();
        }

        return null;
    }

    public List<Map<String, Object>> responseProcess(SearchResponse response, String query) {
        ArrayList<Map<String, Object>> resultList = new ArrayList<>();

        for (SearchHit hit : response.getHits().getHits()) {
            Map<String, Object> sourceAsMap = hit.getSourceAsMap();

            List<Map<String, Object>> results = JSON.parseObject(JSONObject.toJSONString(sourceAsMap.get("results")),
                    new TypeReference<List<Map<String, Object>>>() {
                    });

            for (Map<String, Object> result : results) {
                if (StringUtils.isNotEmpty(result.get("title").toString())){
                    result.put("query", sourceAsMap.get("query"));
                    resultList.add(result);
                }
            }
        }


        return resultList;
    }
}
