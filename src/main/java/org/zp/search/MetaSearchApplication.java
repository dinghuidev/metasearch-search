package org.zp.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author zhangyi
 */
@SpringBootApplication
@EnableAsync
public class MetaSearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(MetaSearchApplication.class, args);
    }
}
