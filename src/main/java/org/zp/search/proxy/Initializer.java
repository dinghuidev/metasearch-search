package org.zp.search.proxy;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.zp.search.utils.TikvClusterInit;
import redis.clients.jedis.JedisCluster;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;


@Log4j2
@Component
public class Initializer {

    private JedisCluster jedisCluster;


    @PostConstruct
    private void init() {

        //init jedis connections
        try {
            String ip = "10.51.3.184";
            int port = 6666;
//            int port = 17369;//tikv b库端口
            TikvClusterInit tikvClusterInit = new TikvClusterInit();
            jedisCluster = tikvClusterInit.getCluster(ip, port);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("init jedis pool error!");
            log.error("[ERROR] init JEDIS pool Error {}", e.getMessage());
            System.exit(0);
        }
    }


    public JedisCluster getJedisCluster() {
        return jedisCluster;
    }

    public static void main(String[] args) {
        JedisCluster jedisCluster;
        try {
            String ip = "10.51.15.109";
            int port = 6666;
//            int port = 17369;//tikv b库端口
            TikvClusterInit tikvClusterInit = new TikvClusterInit();
            jedisCluster = tikvClusterInit.getCluster(ip, port);
            ArrayList<String> objects = new ArrayList<>();
            objects.add("03ee4ee5d000c3dae2e8ad5d1becccc7");
            objects.add("03ee4ee5d000c3dae2e8ad5d1becccc7");
            objects.add("03ee4ee5d000c3dae2e8ad5d1becccc4");
            String[] strings = objects.toArray(new String[0]);
            List<String> res = jedisCluster.mget(strings);
//            String s = jedisCluster.get("03ee4ee5d000c3dae2e8ad5d1becccc7");
            System.out.println(res);
//            RedisGetResult redisGetResult = new RedisGetResult(objects, jedisCluster);
//            ArrayList<DataTemplate> res = redisGetResult.call();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("init jedis pool error!");
            log.error("[ERROR] init JEDIS pool Error {}", e.getMessage());
            System.exit(0);
        }


    }


}
